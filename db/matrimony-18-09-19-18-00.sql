-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 18, 2019 at 02:27 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `matrimony`
--

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_bas_reli_pref`
--

CREATE TABLE `ct_pt_bas_reli_pref` (
  `brp_id` int(11) NOT NULL,
  `brp_profile_id` int(11) NOT NULL,
  `brp_pre_age_st` int(11) DEFAULT NULL,
  `brp_pre_age_end` int(11) DEFAULT NULL,
  `brp_marital_st_id` int(11) DEFAULT NULL,
  `brp_height_st` int(11) DEFAULT NULL,
  `brp_height_end` int(11) DEFAULT NULL,
  `brp_eh_ids` text,
  `brp_dh_ids` text,
  `brp_sh_ids` text,
  `brp_reli_id` int(11) DEFAULT NULL,
  `brp_lan_ids` text,
  `brp_caste_ids` text,
  `brp_chevvai_dosam_id` int(11) DEFAULT NULL,
  `brp_star_ids` text,
  `brp_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `brp_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_family_details`
--

CREATE TABLE `ct_pt_family_details` (
  `fd_id` int(11) NOT NULL,
  `fd_profile_id` int(11) NOT NULL,
  `fd_fv_id` int(11) DEFAULT NULL,
  `fd_ft_id` int(11) DEFAULT NULL,
  `fd_fs_id` int(11) DEFAULT NULL,
  `fd_fas_id` int(11) DEFAULT NULL,
  `fd_mos_id` int(11) DEFAULT NULL,
  `fd_no_of_bros` int(11) DEFAULT NULL,
  `fd_bro_married` int(11) DEFAULT NULL,
  `fd_no_of_sis` int(11) DEFAULT NULL,
  `fd_sis_married` int(11) DEFAULT NULL,
  `fd_family_loc` tinyint(1) NOT NULL DEFAULT '1',
  `fd_family_loc_id` int(11) DEFAULT NULL,
  `fd_family_origin` varchar(250) DEFAULT NULL,
  `fd_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `fd_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_life_style`
--

CREATE TABLE `ct_pt_life_style` (
  `ls_id` int(11) NOT NULL,
  `ls_profile_id` int(11) NOT NULL,
  `ls_hobbies_ids` text,
  `ls_fav_ids` text,
  `ls_sp_fit_ids` text,
  `ls_spk_lan_ids` text,
  `ls_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `ls_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_location`
--

CREATE TABLE `ct_pt_location` (
  `pl_id` int(11) NOT NULL,
  `pl_profile_id` int(11) NOT NULL,
  `pl_live_in_country_id` int(11) DEFAULT NULL,
  `pl_cityzenship_id` int(11) DEFAULT NULL,
  `pl_resi_state_id` int(11) DEFAULT NULL,
  `pl_resi_city_id` int(11) DEFAULT NULL,
  `pl_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `pl_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_loc_pref`
--

CREATE TABLE `ct_pt_loc_pref` (
  `lp_id` int(11) NOT NULL,
  `lp_profile_id` int(11) NOT NULL,
  `lp_lives_in_country_ids` text,
  `lp_state_ids` text,
  `lp_dist_ids` text,
  `lp_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `lp_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_personal_info`
--

CREATE TABLE `ct_pt_personal_info` (
  `pi_id` int(11) NOT NULL,
  `pi_profile_id` int(11) NOT NULL,
  `pi_high_edu_id` int(11) DEFAULT NULL,
  `pi_clg_name` varchar(250) DEFAULT NULL,
  `pi_add_edu_id` int(11) DEFAULT NULL,
  `pi_emp_type_id` int(11) DEFAULT NULL,
  `pi_occ_type_id` int(11) DEFAULT NULL,
  `pi_org_name` varchar(250) DEFAULT NULL,
  `pi_ann_income_curr_id` int(11) DEFAULT NULL,
  `pi_ann_income_id` int(11) DEFAULT NULL,
  `pi_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `pi_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_profile_photo`
--

CREATE TABLE `ct_pt_profile_photo` (
  `pp_id` int(11) NOT NULL,
  `pp_profile_id` int(11) NOT NULL,
  `pp_path` text NOT NULL,
  `pp_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `pp_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_prof_pref`
--

CREATE TABLE `ct_pt_prof_pref` (
  `prof_pref_id` int(11) NOT NULL,
  `prof_pref_profile_id` int(11) NOT NULL,
  `prof_pref_edu_ids` text,
  `prof_pref_occu_ids` text,
  `prof_pref_ann_in_id` int(11) DEFAULT NULL,
  `prof_pref_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `prof_pref_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_reli_info`
--

CREATE TABLE `ct_pt_reli_info` (
  `ri_id` int(11) NOT NULL,
  `ri_profile_id` int(11) NOT NULL,
  `ri_religion_id` int(11) DEFAULT NULL,
  `ri_caste_id` int(11) DEFAULT NULL,
  `ri_sub_caste_id` int(11) DEFAULT NULL,
  `ri_gothra` varchar(250) DEFAULT NULL,
  `ri_start_id` int(11) DEFAULT NULL,
  `ri_raasi_moon_sign_id` int(11) DEFAULT NULL,
  `ri_raasi_zodi_moon_sign_id` int(11) DEFAULT NULL,
  `ri_dosam_id` int(11) DEFAULT NULL,
  `ri_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `ri_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_annual_income`
--

CREATE TABLE `mt_annual_income` (
  `ann_in_id` int(11) NOT NULL,
  `ann_in_curr_id` int(11) NOT NULL,
  `ann_in_range` varchar(250) NOT NULL,
  `ann_in_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `ann_in_cr_uid` int(11) NOT NULL,
  `ann_in_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ann_in_up_uid` int(11) DEFAULT NULL,
  `ann_in_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_body_type`
--

CREATE TABLE `mt_body_type` (
  `bt_id` int(11) NOT NULL,
  `bt_type` varchar(100) NOT NULL,
  `bt_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `bt_cr_uid` int(11) NOT NULL,
  `bt_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bt_up_uid` int(11) DEFAULT NULL,
  `bt_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_body_type`
--

INSERT INTO `mt_body_type` (`bt_id`, `bt_type`, `bt_is_frz`, `bt_cr_uid`, `bt_cr_dt`, `bt_up_uid`, `bt_up_dt`) VALUES
(1, 'Slim', 1, 1, '2019-09-18 16:27:09', NULL, NULL),
(2, 'Athletic', 1, 1, '2019-09-18 16:27:09', NULL, NULL),
(3, 'Average', 1, 1, '2019-09-18 16:27:09', NULL, NULL),
(4, 'Heavy', 1, 1, '2019-09-18 16:27:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_caste`
--

CREATE TABLE `mt_caste` (
  `caste_id` int(11) NOT NULL,
  `caste_type` varchar(100) NOT NULL,
  `caste_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `caste_cr_uid` int(11) NOT NULL,
  `caste_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `caste_up_uid` int(11) DEFAULT NULL,
  `caste_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_complexion`
--

CREATE TABLE `mt_complexion` (
  `cplx_id` int(11) NOT NULL,
  `cplx_type` varchar(100) NOT NULL,
  `cplx_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `cplx_cr_uid` int(11) NOT NULL,
  `cplx_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cplx_up_uid` int(11) DEFAULT NULL,
  `cplx_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_complexion`
--

INSERT INTO `mt_complexion` (`cplx_id`, `cplx_type`, `cplx_is_frz`, `cplx_cr_uid`, `cplx_cr_dt`, `cplx_up_uid`, `cplx_up_dt`) VALUES
(1, 'Very Fair ', 1, 1, '2019-09-18 16:30:23', NULL, NULL),
(2, 'Fair', 1, 1, '2019-09-18 16:30:23', NULL, NULL),
(3, 'Wheatish ', 1, 1, '2019-09-18 16:30:23', NULL, NULL),
(4, 'Wheatish Brown', 1, 1, '2019-09-18 16:30:23', NULL, NULL),
(5, 'Dark', 1, 1, '2019-09-18 16:30:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_currency`
--

CREATE TABLE `mt_currency` (
  `curr_id` int(11) NOT NULL,
  `curr_type` varchar(100) NOT NULL,
  `curr_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `curr_cr_uid` int(11) NOT NULL,
  `curr_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `curr_up_uid` int(11) DEFAULT NULL,
  `curr_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_dosam`
--

CREATE TABLE `mt_dosam` (
  `dosam_id` int(11) NOT NULL,
  `dosam_type` varchar(100) NOT NULL,
  `dosam_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `dosam_cr_uid` int(11) NOT NULL,
  `dosam_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dosam_up_uid` int(11) DEFAULT NULL,
  `dosam_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_dosam`
--

INSERT INTO `mt_dosam` (`dosam_id`, `dosam_type`, `dosam_is_frz`, `dosam_cr_uid`, `dosam_cr_dt`, `dosam_up_uid`, `dosam_up_dt`) VALUES
(1, 'Yes', 1, 1, '2019-09-18 17:12:09', NULL, NULL),
(2, 'No', 1, 1, '2019-09-18 17:12:09', NULL, NULL),
(3, 'Don’t Know', 1, 1, '2019-09-18 17:12:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_drinking_habit`
--

CREATE TABLE `mt_drinking_habit` (
  `dh_id` int(11) NOT NULL,
  `dh_type` varchar(100) NOT NULL,
  `dh_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `dh_cr_uid` int(11) NOT NULL,
  `dh_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dh_up_uid` int(11) DEFAULT NULL,
  `dh_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_drinking_habit`
--

INSERT INTO `mt_drinking_habit` (`dh_id`, `dh_type`, `dh_is_frz`, `dh_cr_uid`, `dh_cr_dt`, `dh_up_uid`, `dh_up_dt`) VALUES
(1, 'Yes', 1, 1, '2019-09-18 16:41:30', NULL, NULL),
(2, 'No', 1, 1, '2019-09-18 16:41:30', NULL, NULL),
(3, 'Drinks Socially', 1, 1, '2019-09-18 16:41:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_eating_habit`
--

CREATE TABLE `mt_eating_habit` (
  `eh_id` int(11) NOT NULL,
  `eh_type` varchar(100) NOT NULL,
  `eh_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `eh_cr_uid` int(11) NOT NULL,
  `eh_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `eh_up_uid` int(11) DEFAULT NULL,
  `eh_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_eating_habit`
--

INSERT INTO `mt_eating_habit` (`eh_id`, `eh_type`, `eh_is_frz`, `eh_cr_uid`, `eh_cr_dt`, `eh_up_uid`, `eh_up_dt`) VALUES
(1, 'Vegetarian', 1, 1, '2019-09-18 16:40:13', NULL, NULL),
(2, 'Non Vegetarian', 1, 1, '2019-09-18 16:40:13', NULL, NULL),
(3, 'Eggetarian', 1, 1, '2019-09-18 16:40:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_edu`
--

CREATE TABLE `mt_edu` (
  `edu_id` int(11) NOT NULL,
  `edu_course` varchar(250) NOT NULL,
  `edu_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `edu_cr_uid` int(11) NOT NULL,
  `edu_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edu_up_uid` int(11) DEFAULT NULL,
  `edu_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_emp_type`
--

CREATE TABLE `mt_emp_type` (
  `emp_type_id` int(11) NOT NULL,
  `emp_type_type` varchar(100) NOT NULL,
  `emp_type_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `emp_type_cr_uid` int(11) NOT NULL,
  `emp_type_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `emp_type_up_uid` int(11) DEFAULT NULL,
  `emp_type_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_emp_type`
--

INSERT INTO `mt_emp_type` (`emp_type_id`, `emp_type_type`, `emp_type_is_frz`, `emp_type_cr_uid`, `emp_type_cr_dt`, `emp_type_up_uid`, `emp_type_up_dt`) VALUES
(1, 'Private Company', 1, 1, '2019-09-18 17:23:58', NULL, NULL),
(2, 'Government / Public Sector', 1, 1, '2019-09-18 17:23:58', NULL, NULL),
(3, 'Defense / Civil Services', 1, 1, '2019-09-18 17:23:58', NULL, NULL),
(4, 'Business / Self Employed', 1, 1, '2019-09-18 17:23:58', NULL, NULL),
(5, 'Non Working', 1, 1, '2019-09-18 17:23:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_family_location`
--

CREATE TABLE `mt_family_location` (
  `f_loc_id` int(11) NOT NULL,
  `f_loc_country_id` int(11) NOT NULL,
  `f_loc_state_id` int(11) NOT NULL,
  `f_loc_city_id` int(11) NOT NULL,
  `f_loc_cr_uid` int(11) NOT NULL,
  `f_loc_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_loc_up_uid` int(11) DEFAULT NULL,
  `f_loc_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_family_status`
--

CREATE TABLE `mt_family_status` (
  `fs_id` int(11) NOT NULL,
  `fs_type` varchar(100) NOT NULL,
  `fs_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `fs_cr_uid` int(11) NOT NULL,
  `fs_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fs_up_uid` int(11) DEFAULT NULL,
  `fs_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_family_status`
--

INSERT INTO `mt_family_status` (`fs_id`, `fs_type`, `fs_is_frz`, `fs_cr_uid`, `fs_cr_dt`, `fs_up_uid`, `fs_up_dt`) VALUES
(1, 'Middle Class', 1, 1, '2019-09-18 17:28:54', NULL, NULL),
(2, 'Upper Middle Class', 1, 1, '2019-09-18 17:28:54', NULL, NULL),
(3, 'Rich', 1, 1, '2019-09-18 17:28:54', NULL, NULL),
(4, 'Affluent', 1, 1, '2019-09-18 17:28:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_family_type`
--

CREATE TABLE `mt_family_type` (
  `ft_id` int(11) NOT NULL,
  `ft_type` varchar(100) NOT NULL,
  `ft_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `ft_cr_uid` int(11) NOT NULL,
  `ft_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ft_up_uid` int(11) DEFAULT NULL,
  `ft_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_family_type`
--

INSERT INTO `mt_family_type` (`ft_id`, `ft_type`, `ft_is_frz`, `ft_cr_uid`, `ft_cr_dt`, `ft_up_uid`, `ft_up_dt`) VALUES
(1, 'Joint Family', 1, 1, '2019-09-18 17:27:23', NULL, NULL),
(2, 'Nuclear Family', 1, 1, '2019-09-18 17:27:23', NULL, NULL),
(3, 'Others', 1, 1, '2019-09-18 17:27:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_family_value`
--

CREATE TABLE `mt_family_value` (
  `fv_id` int(11) NOT NULL,
  `fv_type` varchar(100) NOT NULL,
  `fv_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `fv_cr_uid` int(11) NOT NULL,
  `fv_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fv_up_uid` int(11) DEFAULT NULL,
  `fv_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_family_value`
--

INSERT INTO `mt_family_value` (`fv_id`, `fv_type`, `fv_is_frz`, `fv_cr_uid`, `fv_cr_dt`, `fv_up_uid`, `fv_up_dt`) VALUES
(1, 'Orthodox', 1, 1, '2019-09-18 17:26:17', NULL, NULL),
(2, 'Traditional', 1, 1, '2019-09-18 17:26:17', NULL, NULL),
(3, 'Moderate', 1, 1, '2019-09-18 17:26:17', NULL, NULL),
(4, 'Liberal', 1, 1, '2019-09-18 17:26:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_father_status`
--

CREATE TABLE `mt_father_status` (
  `fas_id` int(11) NOT NULL,
  `fas_type` varchar(100) NOT NULL,
  `fas_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `fas_cr_uid` int(11) NOT NULL,
  `fas_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fas_up_uid` int(11) DEFAULT NULL,
  `fas_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_father_status`
--

INSERT INTO `mt_father_status` (`fas_id`, `fas_type`, `fas_is_frz`, `fas_cr_uid`, `fas_cr_dt`, `fas_up_uid`, `fas_up_dt`) VALUES
(1, 'Employed', 1, 1, '2019-09-18 17:30:34', NULL, NULL),
(2, 'Business Man', 1, 1, '2019-09-18 17:30:34', NULL, NULL),
(3, 'Professional', 1, 1, '2019-09-18 17:30:34', NULL, NULL),
(4, 'Retired', 1, 1, '2019-09-18 17:30:34', NULL, NULL),
(5, 'Not Employed', 1, 1, '2019-09-18 17:30:34', NULL, NULL),
(6, 'Passed Away', 1, 1, '2019-09-18 17:30:34', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_fav_music`
--

CREATE TABLE `mt_fav_music` (
  `fav_mu_id` int(11) NOT NULL,
  `fav_mu_type` varchar(100) NOT NULL,
  `fav_mu_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `fav_mu_cr_uid` int(11) NOT NULL,
  `fav_mu_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fav_mu_up_uid` int(11) DEFAULT NULL,
  `fav_mu_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_fav_music`
--

INSERT INTO `mt_fav_music` (`fav_mu_id`, `fav_mu_type`, `fav_mu_is_frz`, `fav_mu_cr_uid`, `fav_mu_cr_dt`, `fav_mu_up_uid`, `fav_mu_up_dt`) VALUES
(1, 'Film songs', 1, 1, '2019-09-18 17:47:38', NULL, NULL),
(2, 'Indian / Classical Music', 1, 1, '2019-09-18 17:47:38', NULL, NULL),
(3, 'Western Music', 1, 1, '2019-09-18 17:47:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_hobbies_interest`
--

CREATE TABLE `mt_hobbies_interest` (
  `hobbies_id` int(11) NOT NULL,
  `hobbies_type` varchar(100) NOT NULL,
  `hobbies_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `hobbies_cr_uid` int(11) NOT NULL,
  `hobbies_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hobbies_up_uid` int(11) DEFAULT NULL,
  `hobbies_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_hobbies_interest`
--

INSERT INTO `mt_hobbies_interest` (`hobbies_id`, `hobbies_type`, `hobbies_is_frz`, `hobbies_cr_uid`, `hobbies_cr_dt`, `hobbies_up_uid`, `hobbies_up_dt`) VALUES
(1, 'Cooking', 1, 1, '2019-09-18 17:46:36', NULL, NULL),
(2, 'Nature', 1, 1, '2019-09-18 17:46:36', NULL, NULL),
(3, 'Photography', 1, 1, '2019-09-18 17:46:36', NULL, NULL),
(4, 'Dancing', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(5, 'Painting', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(6, 'Pets', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(7, 'Playing Musical Instruments', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(8, 'Gardening / Landscaping', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(9, 'Art / Handicraft', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(10, 'Listening to Music', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(11, 'Movies', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(12, 'Internet Surfing', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(13, 'Traveling', 1, 1, '2019-09-18 17:46:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_language`
--

CREATE TABLE `mt_language` (
  `lan_id` int(11) NOT NULL,
  `lan_type` varchar(100) NOT NULL,
  `lan_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `lan_cr_uid` int(11) NOT NULL,
  `lan_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lan_up_uid` int(11) DEFAULT NULL,
  `lan_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_language`
--

INSERT INTO `mt_language` (`lan_id`, `lan_type`, `lan_is_frz`, `lan_cr_uid`, `lan_cr_dt`, `lan_up_uid`, `lan_up_dt`) VALUES
(1, 'Tamil', 1, 1, '2019-09-18 16:38:50', NULL, NULL),
(2, 'Bengali', 1, 1, '2019-09-18 16:38:50', NULL, NULL),
(3, 'Hindi', 1, 1, '2019-09-18 16:38:50', NULL, NULL),
(4, 'Marathi', 1, 1, '2019-09-18 16:38:50', NULL, NULL),
(5, 'Telugu', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(6, 'Gujarati', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(7, 'Urdu', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(8, 'Kannada', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(9, 'Odia', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(10, 'Malayalam', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(11, 'Punjabi', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(12, 'Assamese', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(13, 'Maithili', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(14, 'Bhili/Bhilodi', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(15, 'Santali', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(16, 'Kashmiri', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(17, 'Gondi', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(18, 'Nepali', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(19, 'Sindhi', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(20, 'Dogri', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(21, 'Konkani', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(22, 'Kurukh', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(23, 'Khandeshi', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(24, 'Tulu', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(25, 'Gondi', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(26, 'Manipuri', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(27, 'Bodo', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(28, 'Khasi', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(29, 'Ho', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(30, 'Mundari', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(31, 'Garo', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(32, 'Tripuri', 1, 1, '2019-09-18 16:38:52', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_marital_status`
--

CREATE TABLE `mt_marital_status` (
  `ms_id` int(11) NOT NULL,
  `ms_type` varchar(100) NOT NULL,
  `ms_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `ms_cr_uid` int(11) NOT NULL,
  `ms_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ms_up_uid` int(11) DEFAULT NULL,
  `ms_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_marital_status`
--

INSERT INTO `mt_marital_status` (`ms_id`, `ms_type`, `ms_is_frz`, `ms_cr_uid`, `ms_cr_dt`, `ms_up_uid`, `ms_up_dt`) VALUES
(1, 'Never Married', 1, 1, '2019-09-18 16:25:40', NULL, NULL),
(2, 'Widowed', 1, 1, '2019-09-18 16:25:40', NULL, NULL),
(3, 'Divorced', 1, 1, '2019-09-18 16:25:40', NULL, NULL),
(4, 'Awaiting Divorce', 1, 1, '2019-09-18 16:25:40', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_mother_status`
--

CREATE TABLE `mt_mother_status` (
  `mos_id` int(11) NOT NULL,
  `mos_type` varchar(100) NOT NULL,
  `mos_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `mos_cr_uid` int(11) NOT NULL,
  `mos_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mos_up_uid` int(11) DEFAULT NULL,
  `mos_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_mother_status`
--

INSERT INTO `mt_mother_status` (`mos_id`, `mos_type`, `mos_is_frz`, `mos_cr_uid`, `mos_cr_dt`, `mos_up_uid`, `mos_up_dt`) VALUES
(1, 'Employed', 1, 1, '2019-09-18 17:31:39', NULL, NULL),
(2, 'Business Women', 1, 1, '2019-09-18 17:31:39', NULL, NULL),
(3, 'Professional', 1, 1, '2019-09-18 17:31:39', NULL, NULL),
(4, 'Retired', 1, 1, '2019-09-18 17:31:39', NULL, NULL),
(5, 'Not Employed', 1, 1, '2019-09-18 17:31:39', NULL, NULL),
(6, 'Passed Away', 1, 1, '2019-09-18 17:31:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_occupation`
--

CREATE TABLE `mt_occupation` (
  `occ_id` int(11) NOT NULL,
  `occ_type` varchar(250) NOT NULL,
  `occ_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `occ_cr_uid` int(11) NOT NULL,
  `occ_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `occ_up_uid` int(11) DEFAULT NULL,
  `occ_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_physical_status`
--

CREATE TABLE `mt_physical_status` (
  `ps_id` int(11) NOT NULL,
  `ps_type` varchar(100) NOT NULL,
  `ps_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `ps_cr_uid` int(11) NOT NULL,
  `ps_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ps_up_uid` int(11) DEFAULT NULL,
  `ps_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_physical_status`
--

INSERT INTO `mt_physical_status` (`ps_id`, `ps_type`, `ps_is_frz`, `ps_cr_uid`, `ps_cr_dt`, `ps_up_uid`, `ps_up_dt`) VALUES
(1, 'Normal', 1, 1, '2019-09-18 16:31:51', NULL, NULL),
(2, 'Physically Challenged', 1, 1, '2019-09-18 16:31:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_premium_package`
--

CREATE TABLE `mt_premium_package` (
  `pp_id` int(11) NOT NULL,
  `pp_desc` text NOT NULL,
  `pp_image_path` varchar(250) NOT NULL,
  `pp_cost` double NOT NULL,
  `pp_discount` double NOT NULL,
  `pp_days` int(11) NOT NULL,
  `pp_mobile_no_count` int(11) NOT NULL,
  `pp_is_frz` int(11) NOT NULL DEFAULT '1',
  `pp_cr_uid` int(11) NOT NULL,
  `pp_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pp_up_uid` int(11) DEFAULT NULL,
  `pp_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_profile`
--

CREATE TABLE `mt_profile` (
  `p_id` int(11) NOT NULL,
  `p_pro_cre_id` int(11) NOT NULL,
  `p_name` varchar(250) NOT NULL,
  `p_dob` date NOT NULL,
  `p_gender` tinyint(1) NOT NULL,
  `p_height` double NOT NULL,
  `p_weight` double NOT NULL,
  `p_mob_no1` varchar(15) NOT NULL,
  `p_mon_no2` varchar(15) DEFAULT NULL,
  `p_email` varchar(100) NOT NULL,
  `p_password` text NOT NULL,
  `p_marital_status_id` int(11) NOT NULL,
  `p_body_type_id` int(11) DEFAULT NULL,
  `p_complexion_id` int(11) DEFAULT NULL,
  `p_physical_status_id` int(11) DEFAULT '1',
  `p_mother_tongue_id` int(11) NOT NULL,
  `p_lan_can_read` tinyint(1) DEFAULT '1',
  `p_eh_ids` text,
  `p_dh_ids` text,
  `p_sh_ids` text,
  `p_about_me` text,
  `p_about_family` text,
  `p_abt_exp_looking_partner` text,
  `p_otp` varchar(10) DEFAULT NULL,
  `p_is_verified` tinyint(1) DEFAULT '2',
  `p_last_login_dt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `p_is_paid_user` tinyint(1) DEFAULT '2',
  `p_pre_package_id` int(11) DEFAULT NULL,
  `p_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_profile_creator`
--

CREATE TABLE `mt_profile_creator` (
  `profile_creator_id` int(11) NOT NULL,
  `profile_creator_type` varchar(100) NOT NULL,
  `profile_creator_is_frz` tinyint(1) DEFAULT '1',
  `profile_creator_cr_uid` int(11) NOT NULL,
  `profile_creator_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `profile_creator_up_uid` int(11) DEFAULT NULL,
  `profile_creator_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_profile_creator`
--

INSERT INTO `mt_profile_creator` (`profile_creator_id`, `profile_creator_type`, `profile_creator_is_frz`, `profile_creator_cr_uid`, `profile_creator_cr_dt`, `profile_creator_up_uid`, `profile_creator_up_dt`) VALUES
(1, 'MySelf', 1, 1, '2019-09-18 16:22:13', NULL, NULL),
(2, 'Relative', 1, 1, '2019-09-18 16:23:02', NULL, NULL),
(3, 'Friend', 1, 1, '2019-09-18 16:23:02', NULL, NULL),
(4, 'Son', 1, 1, '2019-09-18 16:23:02', NULL, NULL),
(5, 'Brother', 1, 1, '2019-09-18 16:23:03', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_rasi_moon_sign`
--

CREATE TABLE `mt_rasi_moon_sign` (
  `rms_id` int(11) NOT NULL,
  `rms_star_id` int(11) NOT NULL,
  `rms_type` varchar(100) NOT NULL,
  `rms_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `rms_cr_uid` int(11) NOT NULL,
  `rms_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rms_up_uid` int(11) DEFAULT NULL,
  `rms_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_religion`
--

CREATE TABLE `mt_religion` (
  `religion_id` int(11) NOT NULL,
  `religion_type` varchar(100) NOT NULL,
  `religion_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `religion_cr_uid` int(11) NOT NULL,
  `religion_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `religion_up_uid` int(11) DEFAULT NULL,
  `religion_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_religion`
--

INSERT INTO `mt_religion` (`religion_id`, `religion_type`, `religion_is_frz`, `religion_cr_uid`, `religion_cr_dt`, `religion_up_uid`, `religion_up_dt`) VALUES
(1, 'Christian', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(2, 'Muslim', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(3, 'Hindu', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(4, 'Sikh', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(5, 'Parsi', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(6, 'Jain', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(7, 'Buddhist', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(8, 'Jewish', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(9, 'Other', 1, 1, '2019-09-18 16:53:24', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_resident_status`
--

CREATE TABLE `mt_resident_status` (
  `rs_id` int(11) NOT NULL,
  `rs_type` varchar(100) NOT NULL,
  `rs_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `rs_cr_uid` int(11) NOT NULL,
  `rs_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rs_up_uid` int(11) DEFAULT NULL,
  `rs_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_resident_status`
--

INSERT INTO `mt_resident_status` (`rs_id`, `rs_type`, `rs_is_frz`, `rs_cr_uid`, `rs_cr_dt`, `rs_up_uid`, `rs_up_dt`) VALUES
(1, 'Permanent Resident', 1, 1, '2019-09-18 17:18:18', NULL, NULL),
(2, 'Work Permit', 1, 1, '2019-09-18 17:18:18', NULL, NULL),
(3, 'Student Visa', 1, 1, '2019-09-18 17:18:18', NULL, NULL),
(4, 'Temporary Visa', 1, 1, '2019-09-18 17:18:18', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_smoking_habit`
--

CREATE TABLE `mt_smoking_habit` (
  `sh_id` int(11) NOT NULL,
  `sh_type` varchar(100) NOT NULL,
  `sh_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `sh_cr_uid` int(11) NOT NULL,
  `sh_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sh_up_uid` int(11) DEFAULT NULL,
  `sh_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_smoking_habit`
--

INSERT INTO `mt_smoking_habit` (`sh_id`, `sh_type`, `sh_is_frz`, `sh_cr_uid`, `sh_cr_dt`, `sh_up_uid`, `sh_up_dt`) VALUES
(1, 'Yes', 1, 1, '2019-09-18 16:42:32', NULL, NULL),
(2, 'No', 1, 1, '2019-09-18 16:42:32', NULL, NULL),
(3, 'Occasionally', 1, 1, '2019-09-18 16:42:32', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_sports_fitness`
--

CREATE TABLE `mt_sports_fitness` (
  `sp_ft_id` int(11) NOT NULL,
  `sp_ft_type` varchar(100) NOT NULL,
  `sp_ft_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `sp_ft_cr_uid` int(11) NOT NULL,
  `sp_ft_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sp_ft_up_uid` int(11) DEFAULT NULL,
  `sp_ft_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_sports_fitness`
--

INSERT INTO `mt_sports_fitness` (`sp_ft_id`, `sp_ft_type`, `sp_ft_is_frz`, `sp_ft_cr_uid`, `sp_ft_cr_dt`, `sp_ft_up_uid`, `sp_ft_up_dt`) VALUES
(1, 'Cricket', 1, 1, '2019-09-18 17:49:43', NULL, NULL),
(2, 'Carrom', 1, 1, '2019-09-18 17:49:43', NULL, NULL),
(3, 'Chess', 1, 1, '2019-09-18 17:49:43', NULL, NULL),
(4, 'Jogging', 1, 1, '2019-09-18 17:49:43', NULL, NULL),
(5, 'Badminton', 1, 1, '2019-09-18 17:49:44', NULL, NULL),
(6, 'Swimming', 1, 1, '2019-09-18 17:49:44', NULL, NULL),
(7, 'Tennis', 1, 1, '2019-09-18 17:49:44', NULL, NULL),
(8, 'FootballOthers', 1, 1, '2019-09-18 17:49:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_star`
--

CREATE TABLE `mt_star` (
  `star_id` int(11) NOT NULL,
  `star_type` varchar(100) NOT NULL,
  `star_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `star_cr_uid` int(11) NOT NULL,
  `star_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `star_up_uid` int(11) DEFAULT NULL,
  `star_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_sub_caste`
--

CREATE TABLE `mt_sub_caste` (
  `sub_caste_id` int(11) NOT NULL,
  `caste_id` int(11) NOT NULL,
  `sub_caste_type` varchar(100) NOT NULL,
  `sub_caste_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `sub_caste_cr_uid` int(11) NOT NULL,
  `sub_caste_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sub_caste_up_uid` int(11) DEFAULT NULL,
  `sub_caste_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_zodiac_star_sign`
--

CREATE TABLE `mt_zodiac_star_sign` (
  `zodiac_id` int(11) NOT NULL,
  `zodiac_type` varchar(100) NOT NULL,
  `zodiac_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `zodiac_cr_uid` int(11) NOT NULL,
  `zodiac_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `zodiac_up_uid` int(11) DEFAULT NULL,
  `zodiac_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `st_profile`
--

CREATE TABLE `st_profile` (
  `sett_id` int(11) NOT NULL,
  `sett_profile_id` varchar(45) NOT NULL,
  `sett_auto_email` tinyint(1) NOT NULL DEFAULT '1',
  `sett_profile_photo_is_hidden` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_mobile_no_viewed`
--

CREATE TABLE `tt_mobile_no_viewed` (
  `mv_id` int(11) NOT NULL,
  `mv_profile_id` int(11) NOT NULL,
  `mv_person_id` int(11) NOT NULL,
  `mv_dt` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_premium_members`
--

CREATE TABLE `tt_premium_members` (
  `pm_id` int(11) NOT NULL,
  `pm_profile_id` int(11) NOT NULL,
  `pm_pp_id` int(11) NOT NULL,
  `pm_pp_cost` double NOT NULL,
  `pm_pp_discount` double NOT NULL,
  `pm_pp_days` int(11) NOT NULL,
  `pm_pp_carryover_ref_id` int(11) DEFAULT NULL,
  `pm_pp_carryover_mobile_no_count` int(11) DEFAULT NULL,
  `pm_pp_mobile_no_count` int(11) NOT NULL,
  `pm_pp_bal_mobile_no_count` int(11) NOT NULL,
  `pm_pp_start_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pm_pp_end_dt` datetime NOT NULL,
  `pm_pp_telecaller_id` int(11) DEFAULT NULL,
  `pm_pp_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_pro_ig_by_me`
--

CREATE TABLE `tt_pro_ig_by_me` (
  `pig_id` int(11) NOT NULL,
  `pig_profile_id` int(11) NOT NULL,
  `pig_person_id` int(11) NOT NULL,
  `pig_dt` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_request`
--

CREATE TABLE `tt_request` (
  `req_id` int(11) NOT NULL,
  `req_profile_id` int(11) NOT NULL,
  `req_person_id` int(11) NOT NULL,
  `req_is_accepted` tinyint(1) NOT NULL DEFAULT '1',
  `req_acc_dt` datetime DEFAULT NULL,
  `req_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_shortlisted_profile`
--

CREATE TABLE `tt_shortlisted_profile` (
  `short_id` int(11) NOT NULL,
  `short_profile_id` int(11) DEFAULT NULL,
  `short_person_id` int(11) NOT NULL,
  `short_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_viewed_my_profile`
--

CREATE TABLE `tt_viewed_my_profile` (
  `vmp_id` int(11) NOT NULL,
  `vmp_profile_id` int(11) NOT NULL,
  `vmp_person_id` int(11) NOT NULL,
  `vmp_is_contacted` tinyint(1) NOT NULL DEFAULT '2',
  `vmp_viewed_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ct_pt_bas_reli_pref`
--
ALTER TABLE `ct_pt_bas_reli_pref`
  ADD PRIMARY KEY (`brp_id`);

--
-- Indexes for table `ct_pt_family_details`
--
ALTER TABLE `ct_pt_family_details`
  ADD PRIMARY KEY (`fd_id`);

--
-- Indexes for table `ct_pt_life_style`
--
ALTER TABLE `ct_pt_life_style`
  ADD PRIMARY KEY (`ls_id`);

--
-- Indexes for table `ct_pt_location`
--
ALTER TABLE `ct_pt_location`
  ADD PRIMARY KEY (`pl_id`);

--
-- Indexes for table `ct_pt_loc_pref`
--
ALTER TABLE `ct_pt_loc_pref`
  ADD PRIMARY KEY (`lp_id`);

--
-- Indexes for table `ct_pt_personal_info`
--
ALTER TABLE `ct_pt_personal_info`
  ADD PRIMARY KEY (`pi_id`);

--
-- Indexes for table `ct_pt_profile_photo`
--
ALTER TABLE `ct_pt_profile_photo`
  ADD PRIMARY KEY (`pp_id`);

--
-- Indexes for table `ct_pt_prof_pref`
--
ALTER TABLE `ct_pt_prof_pref`
  ADD PRIMARY KEY (`prof_pref_id`);

--
-- Indexes for table `ct_pt_reli_info`
--
ALTER TABLE `ct_pt_reli_info`
  ADD PRIMARY KEY (`ri_id`);

--
-- Indexes for table `mt_annual_income`
--
ALTER TABLE `mt_annual_income`
  ADD PRIMARY KEY (`ann_in_id`);

--
-- Indexes for table `mt_body_type`
--
ALTER TABLE `mt_body_type`
  ADD PRIMARY KEY (`bt_id`);

--
-- Indexes for table `mt_caste`
--
ALTER TABLE `mt_caste`
  ADD PRIMARY KEY (`caste_id`);

--
-- Indexes for table `mt_complexion`
--
ALTER TABLE `mt_complexion`
  ADD PRIMARY KEY (`cplx_id`);

--
-- Indexes for table `mt_currency`
--
ALTER TABLE `mt_currency`
  ADD PRIMARY KEY (`curr_id`);

--
-- Indexes for table `mt_dosam`
--
ALTER TABLE `mt_dosam`
  ADD PRIMARY KEY (`dosam_id`);

--
-- Indexes for table `mt_drinking_habit`
--
ALTER TABLE `mt_drinking_habit`
  ADD PRIMARY KEY (`dh_id`);

--
-- Indexes for table `mt_eating_habit`
--
ALTER TABLE `mt_eating_habit`
  ADD PRIMARY KEY (`eh_id`);

--
-- Indexes for table `mt_edu`
--
ALTER TABLE `mt_edu`
  ADD PRIMARY KEY (`edu_id`);

--
-- Indexes for table `mt_emp_type`
--
ALTER TABLE `mt_emp_type`
  ADD PRIMARY KEY (`emp_type_id`);

--
-- Indexes for table `mt_family_location`
--
ALTER TABLE `mt_family_location`
  ADD PRIMARY KEY (`f_loc_id`);

--
-- Indexes for table `mt_family_status`
--
ALTER TABLE `mt_family_status`
  ADD PRIMARY KEY (`fs_id`);

--
-- Indexes for table `mt_family_type`
--
ALTER TABLE `mt_family_type`
  ADD PRIMARY KEY (`ft_id`);

--
-- Indexes for table `mt_family_value`
--
ALTER TABLE `mt_family_value`
  ADD PRIMARY KEY (`fv_id`);

--
-- Indexes for table `mt_father_status`
--
ALTER TABLE `mt_father_status`
  ADD PRIMARY KEY (`fas_id`);

--
-- Indexes for table `mt_fav_music`
--
ALTER TABLE `mt_fav_music`
  ADD PRIMARY KEY (`fav_mu_id`);

--
-- Indexes for table `mt_hobbies_interest`
--
ALTER TABLE `mt_hobbies_interest`
  ADD PRIMARY KEY (`hobbies_id`);

--
-- Indexes for table `mt_language`
--
ALTER TABLE `mt_language`
  ADD PRIMARY KEY (`lan_id`);

--
-- Indexes for table `mt_marital_status`
--
ALTER TABLE `mt_marital_status`
  ADD PRIMARY KEY (`ms_id`);

--
-- Indexes for table `mt_mother_status`
--
ALTER TABLE `mt_mother_status`
  ADD PRIMARY KEY (`mos_id`);

--
-- Indexes for table `mt_occupation`
--
ALTER TABLE `mt_occupation`
  ADD PRIMARY KEY (`occ_id`);

--
-- Indexes for table `mt_physical_status`
--
ALTER TABLE `mt_physical_status`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `mt_premium_package`
--
ALTER TABLE `mt_premium_package`
  ADD PRIMARY KEY (`pp_id`,`pp_discount`);

--
-- Indexes for table `mt_profile`
--
ALTER TABLE `mt_profile`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `mt_profile_creator`
--
ALTER TABLE `mt_profile_creator`
  ADD PRIMARY KEY (`profile_creator_id`);

--
-- Indexes for table `mt_rasi_moon_sign`
--
ALTER TABLE `mt_rasi_moon_sign`
  ADD PRIMARY KEY (`rms_id`);

--
-- Indexes for table `mt_religion`
--
ALTER TABLE `mt_religion`
  ADD PRIMARY KEY (`religion_id`);

--
-- Indexes for table `mt_resident_status`
--
ALTER TABLE `mt_resident_status`
  ADD PRIMARY KEY (`rs_id`);

--
-- Indexes for table `mt_smoking_habit`
--
ALTER TABLE `mt_smoking_habit`
  ADD PRIMARY KEY (`sh_id`);

--
-- Indexes for table `mt_sports_fitness`
--
ALTER TABLE `mt_sports_fitness`
  ADD PRIMARY KEY (`sp_ft_id`);

--
-- Indexes for table `mt_star`
--
ALTER TABLE `mt_star`
  ADD PRIMARY KEY (`star_id`);

--
-- Indexes for table `mt_sub_caste`
--
ALTER TABLE `mt_sub_caste`
  ADD PRIMARY KEY (`sub_caste_id`);

--
-- Indexes for table `mt_zodiac_star_sign`
--
ALTER TABLE `mt_zodiac_star_sign`
  ADD PRIMARY KEY (`zodiac_id`);

--
-- Indexes for table `st_profile`
--
ALTER TABLE `st_profile`
  ADD PRIMARY KEY (`sett_id`);

--
-- Indexes for table `tt_mobile_no_viewed`
--
ALTER TABLE `tt_mobile_no_viewed`
  ADD PRIMARY KEY (`mv_id`);

--
-- Indexes for table `tt_premium_members`
--
ALTER TABLE `tt_premium_members`
  ADD PRIMARY KEY (`pm_id`);

--
-- Indexes for table `tt_pro_ig_by_me`
--
ALTER TABLE `tt_pro_ig_by_me`
  ADD PRIMARY KEY (`pig_id`);

--
-- Indexes for table `tt_request`
--
ALTER TABLE `tt_request`
  ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `tt_shortlisted_profile`
--
ALTER TABLE `tt_shortlisted_profile`
  ADD PRIMARY KEY (`short_id`);

--
-- Indexes for table `tt_viewed_my_profile`
--
ALTER TABLE `tt_viewed_my_profile`
  ADD PRIMARY KEY (`vmp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ct_pt_bas_reli_pref`
--
ALTER TABLE `ct_pt_bas_reli_pref`
  MODIFY `brp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_family_details`
--
ALTER TABLE `ct_pt_family_details`
  MODIFY `fd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_life_style`
--
ALTER TABLE `ct_pt_life_style`
  MODIFY `ls_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_location`
--
ALTER TABLE `ct_pt_location`
  MODIFY `pl_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_loc_pref`
--
ALTER TABLE `ct_pt_loc_pref`
  MODIFY `lp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_personal_info`
--
ALTER TABLE `ct_pt_personal_info`
  MODIFY `pi_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_profile_photo`
--
ALTER TABLE `ct_pt_profile_photo`
  MODIFY `pp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_prof_pref`
--
ALTER TABLE `ct_pt_prof_pref`
  MODIFY `prof_pref_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_reli_info`
--
ALTER TABLE `ct_pt_reli_info`
  MODIFY `ri_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_annual_income`
--
ALTER TABLE `mt_annual_income`
  MODIFY `ann_in_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_body_type`
--
ALTER TABLE `mt_body_type`
  MODIFY `bt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mt_caste`
--
ALTER TABLE `mt_caste`
  MODIFY `caste_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_complexion`
--
ALTER TABLE `mt_complexion`
  MODIFY `cplx_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mt_currency`
--
ALTER TABLE `mt_currency`
  MODIFY `curr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_dosam`
--
ALTER TABLE `mt_dosam`
  MODIFY `dosam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_drinking_habit`
--
ALTER TABLE `mt_drinking_habit`
  MODIFY `dh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_eating_habit`
--
ALTER TABLE `mt_eating_habit`
  MODIFY `eh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_edu`
--
ALTER TABLE `mt_edu`
  MODIFY `edu_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_emp_type`
--
ALTER TABLE `mt_emp_type`
  MODIFY `emp_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mt_family_location`
--
ALTER TABLE `mt_family_location`
  MODIFY `f_loc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_family_status`
--
ALTER TABLE `mt_family_status`
  MODIFY `fs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mt_family_type`
--
ALTER TABLE `mt_family_type`
  MODIFY `ft_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_family_value`
--
ALTER TABLE `mt_family_value`
  MODIFY `fv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mt_father_status`
--
ALTER TABLE `mt_father_status`
  MODIFY `fas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mt_fav_music`
--
ALTER TABLE `mt_fav_music`
  MODIFY `fav_mu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_hobbies_interest`
--
ALTER TABLE `mt_hobbies_interest`
  MODIFY `hobbies_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `mt_language`
--
ALTER TABLE `mt_language`
  MODIFY `lan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `mt_marital_status`
--
ALTER TABLE `mt_marital_status`
  MODIFY `ms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mt_mother_status`
--
ALTER TABLE `mt_mother_status`
  MODIFY `mos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mt_occupation`
--
ALTER TABLE `mt_occupation`
  MODIFY `occ_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_physical_status`
--
ALTER TABLE `mt_physical_status`
  MODIFY `ps_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mt_premium_package`
--
ALTER TABLE `mt_premium_package`
  MODIFY `pp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_profile`
--
ALTER TABLE `mt_profile`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_profile_creator`
--
ALTER TABLE `mt_profile_creator`
  MODIFY `profile_creator_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mt_rasi_moon_sign`
--
ALTER TABLE `mt_rasi_moon_sign`
  MODIFY `rms_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_religion`
--
ALTER TABLE `mt_religion`
  MODIFY `religion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mt_resident_status`
--
ALTER TABLE `mt_resident_status`
  MODIFY `rs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mt_smoking_habit`
--
ALTER TABLE `mt_smoking_habit`
  MODIFY `sh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_sports_fitness`
--
ALTER TABLE `mt_sports_fitness`
  MODIFY `sp_ft_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mt_star`
--
ALTER TABLE `mt_star`
  MODIFY `star_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_sub_caste`
--
ALTER TABLE `mt_sub_caste`
  MODIFY `sub_caste_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_zodiac_star_sign`
--
ALTER TABLE `mt_zodiac_star_sign`
  MODIFY `zodiac_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_profile`
--
ALTER TABLE `st_profile`
  MODIFY `sett_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_mobile_no_viewed`
--
ALTER TABLE `tt_mobile_no_viewed`
  MODIFY `mv_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_premium_members`
--
ALTER TABLE `tt_premium_members`
  MODIFY `pm_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_pro_ig_by_me`
--
ALTER TABLE `tt_pro_ig_by_me`
  MODIFY `pig_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_request`
--
ALTER TABLE `tt_request`
  MODIFY `req_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_viewed_my_profile`
--
ALTER TABLE `tt_viewed_my_profile`
  MODIFY `vmp_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
