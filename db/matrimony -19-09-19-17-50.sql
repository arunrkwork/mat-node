-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 19, 2019 at 02:20 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `matrimony`
--

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_bas_reli_pref`
--

CREATE TABLE `ct_pt_bas_reli_pref` (
  `brp_id` int(11) NOT NULL,
  `brp_profile_id` int(11) NOT NULL,
  `brp_pre_age_st` int(11) DEFAULT NULL,
  `brp_pre_age_end` int(11) DEFAULT NULL,
  `brp_marital_st_id` int(11) DEFAULT NULL,
  `brp_height_st` int(11) DEFAULT NULL,
  `brp_height_end` int(11) DEFAULT NULL,
  `brp_eh_ids` text,
  `brp_dh_ids` text,
  `brp_sh_ids` text,
  `brp_reli_id` int(11) DEFAULT NULL,
  `brp_lan_ids` text,
  `brp_caste_ids` text,
  `brp_chevvai_dosam_id` int(11) DEFAULT NULL,
  `brp_star_ids` text,
  `brp_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `brp_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_family_details`
--

CREATE TABLE `ct_pt_family_details` (
  `fd_id` int(11) NOT NULL,
  `fd_profile_id` int(11) NOT NULL,
  `fd_fv_id` int(11) DEFAULT NULL,
  `fd_ft_id` int(11) DEFAULT NULL,
  `fd_fs_id` int(11) DEFAULT NULL,
  `fd_fas_id` int(11) DEFAULT NULL,
  `fd_mos_id` int(11) DEFAULT NULL,
  `fd_no_of_bros` int(11) DEFAULT NULL,
  `fd_bro_married` int(11) DEFAULT NULL,
  `fd_no_of_sis` int(11) DEFAULT NULL,
  `fd_sis_married` int(11) DEFAULT NULL,
  `fd_family_loc` tinyint(1) NOT NULL DEFAULT '1',
  `fd_family_loc_id` int(11) DEFAULT NULL,
  `fd_family_origin` varchar(250) DEFAULT NULL,
  `fd_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `fd_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_life_style`
--

CREATE TABLE `ct_pt_life_style` (
  `ls_id` int(11) NOT NULL,
  `ls_profile_id` int(11) NOT NULL,
  `ls_hobbies_ids` text,
  `ls_fav_ids` text,
  `ls_sp_fit_ids` text,
  `ls_spk_lan_ids` text,
  `ls_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `ls_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_location`
--

CREATE TABLE `ct_pt_location` (
  `pl_id` int(11) NOT NULL,
  `pl_profile_id` int(11) NOT NULL,
  `pl_live_in_country_id` int(11) DEFAULT NULL,
  `pl_cityzenship_id` int(11) DEFAULT NULL,
  `pl_resi_state_id` int(11) DEFAULT NULL,
  `pl_resi_city_id` int(11) DEFAULT NULL,
  `pl_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `pl_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_loc_pref`
--

CREATE TABLE `ct_pt_loc_pref` (
  `lp_id` int(11) NOT NULL,
  `lp_profile_id` int(11) NOT NULL,
  `lp_lives_in_country_ids` text,
  `lp_state_ids` text,
  `lp_dist_ids` text,
  `lp_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `lp_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_personal_info`
--

CREATE TABLE `ct_pt_personal_info` (
  `pi_id` int(11) NOT NULL,
  `pi_profile_id` int(11) NOT NULL,
  `pi_high_edu_id` int(11) DEFAULT NULL,
  `pi_clg_name` varchar(250) DEFAULT NULL,
  `pi_add_edu_id` int(11) DEFAULT NULL,
  `pi_emp_type_id` int(11) DEFAULT NULL,
  `pi_occ_type_id` int(11) DEFAULT NULL,
  `pi_org_name` varchar(250) DEFAULT NULL,
  `pi_ann_income_curr_id` int(11) DEFAULT NULL,
  `pi_ann_income_id` int(11) DEFAULT NULL,
  `pi_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `pi_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_profile_photo`
--

CREATE TABLE `ct_pt_profile_photo` (
  `pp_id` int(11) NOT NULL,
  `pp_profile_id` int(11) NOT NULL,
  `pp_path` text NOT NULL,
  `pp_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `pp_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_prof_pref`
--

CREATE TABLE `ct_pt_prof_pref` (
  `prof_pref_id` int(11) NOT NULL,
  `prof_pref_profile_id` int(11) NOT NULL,
  `prof_pref_edu_ids` text,
  `prof_pref_occu_ids` text,
  `prof_pref_ann_in_id` int(11) DEFAULT NULL,
  `prof_pref_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `prof_pref_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ct_pt_reli_info`
--

CREATE TABLE `ct_pt_reli_info` (
  `ri_id` int(11) NOT NULL,
  `ri_profile_id` int(11) NOT NULL,
  `ri_religion_id` int(11) DEFAULT NULL,
  `ri_caste_id` int(11) DEFAULT NULL,
  `ri_sub_caste_id` int(11) DEFAULT NULL,
  `ri_gothra` varchar(250) DEFAULT NULL,
  `ri_start_id` int(11) DEFAULT NULL,
  `ri_raasi_moon_sign_id` int(11) DEFAULT NULL,
  `ri_raasi_zodi_moon_sign_id` int(11) DEFAULT NULL,
  `ri_dosam_id` int(11) DEFAULT NULL,
  `ri_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `ri_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_annual_income`
--

CREATE TABLE `mt_annual_income` (
  `ann_in_id` int(11) NOT NULL,
  `ann_in_curr_id` int(11) NOT NULL,
  `ann_in_range` varchar(250) NOT NULL,
  `ann_in_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `ann_in_cr_uid` int(11) NOT NULL,
  `ann_in_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ann_in_up_uid` int(11) DEFAULT NULL,
  `ann_in_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_body_type`
--

CREATE TABLE `mt_body_type` (
  `bt_id` int(11) NOT NULL,
  `bt_type` varchar(100) NOT NULL,
  `bt_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `bt_cr_uid` int(11) NOT NULL,
  `bt_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bt_up_uid` int(11) DEFAULT NULL,
  `bt_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_body_type`
--

INSERT INTO `mt_body_type` (`bt_id`, `bt_type`, `bt_is_frz`, `bt_cr_uid`, `bt_cr_dt`, `bt_up_uid`, `bt_up_dt`) VALUES
(1, 'Slim', 1, 1, '2019-09-18 16:27:09', NULL, NULL),
(2, 'Athletic', 1, 1, '2019-09-18 16:27:09', NULL, NULL),
(3, 'Average', 1, 1, '2019-09-18 16:27:09', NULL, NULL),
(4, 'Heavy', 1, 1, '2019-09-18 16:27:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_caste`
--

CREATE TABLE `mt_caste` (
  `caste_id` int(11) NOT NULL,
  `caste_reli_id` int(11) NOT NULL,
  `caste_type` varchar(100) NOT NULL,
  `caste_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `caste_cr_uid` int(11) NOT NULL,
  `caste_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `caste_up_uid` int(11) DEFAULT NULL,
  `caste_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_caste`
--

INSERT INTO `mt_caste` (`caste_id`, `caste_reli_id`, `caste_type`, `caste_is_frz`, `caste_cr_uid`, `caste_cr_dt`, `caste_up_uid`, `caste_up_dt`) VALUES
(1, 1, 'Anglo-Indian', 1, 1, '2019-09-19 12:55:08', NULL, NULL),
(2, 1, 'Apostolic', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(3, 1, 'Assembly of God (AG)', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(4, 1, 'Baptist', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(5, 1, 'Born Again', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(6, 1, 'Brethren', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(7, 1, 'Calvinist', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(8, 1, 'Chaldean Syrian (Assyrian)', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(9, 1, 'Church of Christ', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(10, 1, 'Church of God', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(11, 1, 'Church of North India', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(12, 1, 'Church of South India', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(13, 1, 'Congregational', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(14, 1, 'Evangelist', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(15, 1, 'Jacobite', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(16, 1, 'Jehovahs Witnesses', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(17, 1, 'Knanaya', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(18, 1, 'Knanaya Catholic', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(19, 1, 'Knanaya Jacobite', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(20, 1, 'Latin Catholic', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(21, 1, 'Latter day saints', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(22, 1, 'Lutheran', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(23, 1, 'Malabar Independent Syrian Church', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(24, 1, 'Malankara Catholic', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(25, 1, 'Marthoma', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(26, 1, 'Melkite', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(27, 1, 'Mennonite', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(28, 1, 'Methodist', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(29, 1, 'Moravian', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(30, 1, 'Orthodox', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(31, 1, 'Pentecost', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(32, 1, 'Presbyterian', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(33, 1, 'Protestant', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(34, 1, 'Reformed Baptist', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(35, 1, 'Reformed Presbyterian', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(36, 1, 'Roman Catholic', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(37, 1, 'Seventh day Adventist', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(38, 1, 'St. Thomas Evangelical', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(39, 1, 'Syrian Catholic', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(40, 1, 'Syrian Jacobite', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(41, 1, 'Syrian Orthodox', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(42, 1, 'Syro Malabar', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(43, 1, 'Christian - Others', 1, 1, '2019-09-19 12:56:51', NULL, NULL),
(44, 3, 'Hindu Castes', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(45, 3, '96K Kokanastha', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(46, 3, 'Adi Andhra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(47, 3, 'Adi Dravida', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(48, 3, 'Adi Karnataka', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(49, 3, 'Agarwal', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(50, 3, 'Agnikula Kshatriya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(51, 3, 'Agri', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(52, 3, 'Ahom', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(53, 3, 'Ambalavasi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(54, 3, 'Arekatica', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(55, 3, 'Arora', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(56, 3, 'Arunthathiyar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(57, 3, 'Arya Vysya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(58, 3, 'Aryasamaj', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(59, 3, 'Ayyaraka', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(60, 3, 'Badaga', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(61, 3, 'Baghel/Pal/Gaderiya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(62, 3, 'Bahi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(63, 3, 'Baidya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(64, 3, 'Baishnab', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(65, 3, 'Baishya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(66, 3, 'Bajantri', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(67, 3, 'Balija', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(68, 3, 'Balija - Naidu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(69, 3, 'Banik', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(70, 3, 'Baniya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(71, 3, 'Bari', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(72, 3, 'Barnwal', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(73, 3, 'Barujibi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(74, 3, 'Bengali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(75, 3, 'Besta', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(76, 3, 'Bhandari', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(77, 3, 'Bhatia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(78, 3, 'Bhatraju', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(79, 3, 'Bhavsar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(80, 3, 'Bhovi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(81, 3, 'Billava', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(82, 3, 'Boya/Nayak/Naik', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(83, 3, 'Boyer', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(84, 3, 'Brahmbatt', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(85, 3, 'Brahmin', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(86, 3, 'Brahmin - Anavil', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(87, 3, 'Brahmin - Audichya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(88, 3, 'Brahmin - Barendra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(89, 3, 'Brahmin - Bengali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(90, 3, 'Brahmin - Bhatt', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(91, 3, 'Brahmin - Bhumihar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(92, 3, 'Brahmin - Brahmbhatt', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(93, 3, 'Brahmin - Dadhich/Dadheech', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(94, 3, 'Brahmin - Daivadnya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(95, 3, 'Brahmin - Danua', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(96, 3, 'Brahmin - Deshastha', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(97, 3, 'Brahmin - Dhiman', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(98, 3, 'Brahmin - Dravida', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(99, 3, 'Brahmin - Embrandiri', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(100, 3, 'Brahmin - Garhwali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(101, 3, 'Brahmin - Goswami', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(102, 3, 'Brahmin - Gour', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(103, 3, 'Brahmin - Gowd Saraswat', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(104, 3, 'Brahmin - Gurukkal', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(105, 3, 'Brahmin - Halua', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(106, 3, 'Brahmin - Havyaka', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(107, 3, 'Brahmin - Himachali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(108, 3, 'Brahmin - Hoysala', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(109, 3, 'Brahmin - Iyengar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(110, 3, 'Brahmin - Iyer', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(111, 3, 'Brahmin - Jangid', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(112, 3, 'Brahmin - Jhadua', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(113, 3, 'Brahmin - Jhijhotiya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(114, 3, 'Brahmin - Kannada Madhva', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(115, 3, 'Brahmin - Kanyakubja', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(116, 3, 'Brahmin - Karhade', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(117, 3, 'Brahmin - Kashmiri Pandit', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(118, 3, 'Brahmin - Kokanastha', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(119, 3, 'Brahmin - Kota', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(120, 3, 'Brahmin - Kulin', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(121, 3, 'Brahmin - Kumaoni', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(122, 3, 'Brahmin - Madhwa', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(123, 3, 'Brahmin - Maharashtrian', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(124, 3, 'Brahmin - Maithili', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(125, 3, 'Brahmin - Modh', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(126, 3, 'Brahmin - Mohyal', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(127, 3, 'Brahmin - Nagar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(128, 3, 'Brahmin - Namboodiri', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(129, 3, 'Brahmin - Niyogi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(130, 3, 'Brahmin - Niyogi Nandavariki', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(131, 3, 'Brahmin - Other', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(132, 3, 'Brahmin - Paliwal', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(133, 3, 'Brahmin - Panda', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(134, 3, 'Brahmin - Pareek', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(135, 3, 'Brahmin - Punjabi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(136, 3, 'Brahmin - Pushkarna', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(137, 3, 'Brahmin - Rarhi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(138, 3, 'Brahmin - Rudraj', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(139, 3, 'Brahmin - Sakaldwipi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(140, 3, 'Brahmin - Sanadya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(141, 3, 'Brahmin - Sanketi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(142, 3, 'Brahmin - Saraswat', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(143, 3, 'Brahmin - Saryuparin', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(144, 3, 'Brahmin - Shivhalli', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(145, 3, 'Brahmin - Shrimali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(146, 3, 'Brahmin - Smartha', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(147, 3, 'Brahmin - Sri Vaishnava', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(148, 3, 'Brahmin - Stanika', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(149, 3, 'Brahmin - Tyagi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(150, 3, 'Brahmin - Vaidiki', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(151, 3, 'Brahmin - Vaikhanasa', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(152, 3, 'Brahmin - Velanadu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(153, 3, 'Brahmin - Viswabrahmin', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(154, 3, 'Brahmin - Vyas', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(155, 3, 'Brahmo', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(156, 3, 'Buddar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(157, 3, 'Bunt (Shetty)', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(158, 3, 'CKP', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(159, 3, 'Chalawadi Holeya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(160, 3, 'Chambhar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(161, 3, 'Chandravanshi Kahar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(162, 3, 'Chasa', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(163, 3, 'Chattada Sri Vaishnava', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(164, 3, 'Chaudary', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(165, 3, 'Chaurasia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(166, 3, 'Chekkala - Nair', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(167, 3, 'Chennadasar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(168, 3, 'Cheramar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(169, 3, 'Chettiar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(170, 3, 'Chhetri', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(171, 3, 'Chippolu/Mera', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(172, 3, 'Coorgi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(173, 3, 'Devadiga', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(174, 3, 'Devanga', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(175, 3, 'Devar/Thevar/Mukkulathor', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(176, 3, 'Devendra Kula Vellalar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(177, 3, 'Dhangar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(178, 3, 'Dheevara', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(179, 3, 'Dhiman', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(180, 3, 'Dhoba', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(181, 3, 'Dhobi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(182, 3, 'Digambar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(183, 3, 'Dommala', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(184, 3, 'Ediga', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(185, 3, 'Ezhava', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(186, 3, 'Ezhuthachan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(187, 3, 'Gabit', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(188, 3, 'Ganakar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(189, 3, 'Gandla', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(190, 3, 'Ganiga', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(191, 3, 'Garhwali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(192, 3, 'Gatti', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(193, 3, 'Gavali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(194, 3, 'Gavara', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(195, 3, 'Ghumar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(196, 3, 'Goala', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(197, 3, 'Goan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(198, 3, 'Goswami', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(199, 3, 'Goud', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(200, 3, 'Gounder', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(201, 3, 'Gowda', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(202, 3, 'Gramani', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(203, 3, 'Gudia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(204, 3, 'Gujarati', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(205, 3, 'Gujjar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(206, 3, 'Gupta', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(207, 3, 'Guptan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(208, 3, 'Gurjar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(209, 3, 'Hegde', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(210, 3, 'Helava', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(211, 3, 'Hugar (Jeer)', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(212, 3, 'Intercaste', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(213, 3, 'Jaalari', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(214, 3, 'Jaiswal', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(215, 3, 'Jandra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(216, 3, 'Jangam', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(217, 3, 'Jat', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(218, 3, 'Jatav', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(219, 3, 'Jetty Malla', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(220, 3, 'Kachara', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(221, 3, 'Kaibarta', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(222, 3, 'Kakkalan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(223, 3, 'Kalar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(224, 3, 'Kalinga', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(225, 3, 'Kalinga Vysya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(226, 3, 'Kalita', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(227, 3, 'Kalwar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(228, 3, 'Kamboj', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(229, 3, 'Kamma', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(230, 3, 'Kammala', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(231, 3, 'Kaniyan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(232, 3, 'Kannada Mogaveera', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(233, 3, 'Kansari', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(234, 3, 'Kapu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(235, 3, 'Kapu Naidu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(236, 3, 'Karana', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(237, 3, 'Karmakar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(238, 3, 'Kartha', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(239, 3, 'Karuneegar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(240, 3, 'Kasar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(241, 3, 'Kashyap', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(242, 3, 'Kavuthiyya/Ezhavathy', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(243, 3, 'Kayastha', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(244, 3, 'Khandayat', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(245, 3, 'Khandelwal', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(246, 3, 'Kharwar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(247, 3, 'Khatik', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(248, 3, 'Khatri', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(249, 3, 'Koli', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(250, 3, 'Kongu Vellala Gounder', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(251, 3, 'Konkani', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(252, 3, 'Korama', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(253, 3, 'Kori', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(254, 3, 'Koshti', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(255, 3, 'Krishnavaka', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(256, 3, 'Kshatriya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(257, 3, 'Kshatriya - Bhavasar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(258, 3, 'Kshatriya/Raju/Varma', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(259, 3, 'Kudumbi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(260, 3, 'Kulal', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(261, 3, 'Kulalar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(262, 3, 'Kulita', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(263, 3, 'Kumawat', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(264, 3, 'Kumbara', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(265, 3, 'Kumbhakar/Kumbhar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(266, 3, 'Kumhar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(267, 3, 'Kummari', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(268, 3, 'Kunbi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(269, 3, 'Kurava', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(270, 3, 'Kuravan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(271, 3, 'Kurmi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(272, 3, 'Kuruba', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(273, 3, 'Kuruhina Shetty', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(274, 3, 'Kurumbar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(275, 3, 'Kurup', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(276, 3, 'Kushwaha', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(277, 3, 'Kutchi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(278, 3, 'Lambadi/Banjara', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(279, 3, 'Lambani', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(280, 3, 'Leva Patil', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(281, 3, 'Lingayath', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(282, 3, 'Lohana', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(283, 3, 'Lohar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(284, 3, 'Loniya/Lonia/Lunia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(285, 3, 'Lubana', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(286, 3, 'Madiga', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(287, 3, 'Mahajan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(288, 3, 'Mahar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(289, 3, 'Maharashtrian', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(290, 3, 'Mahendra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(291, 3, 'Maheshwari', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(292, 3, 'Mahindra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(293, 3, 'Mahishya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(294, 3, 'Majabi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(295, 3, 'Mala', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(296, 3, 'Malayalee Variar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(297, 3, 'Mali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(298, 3, 'Mangalorean', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(299, 3, 'Maniyani', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(300, 3, 'Mannadiar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(301, 3, 'Mannan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(302, 3, 'Mapila', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(303, 3, 'Marar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(304, 3, 'Maratha', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(305, 3, 'Maratha - Gomantak', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(306, 3, 'Maruthuvar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(307, 3, 'Marvar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(308, 3, 'Marwari', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(309, 3, 'Matang', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(310, 3, 'Maurya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(311, 3, 'Meda', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(312, 3, 'Medara', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(313, 3, 'Meena', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(314, 3, 'Meenavar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(315, 3, 'Mehra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(316, 3, 'Menon', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(317, 3, 'Meru Darji', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(318, 3, 'Modak', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(319, 3, 'Mogaveera', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(320, 3, 'Monchi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(321, 3, 'Mudaliar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(322, 3, 'Mudaliar - Arcot', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(323, 3, 'Mudaliar - Saiva', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(324, 3, 'Mudaliar - Senguntha', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(325, 3, 'Mudiraj', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(326, 3, 'Munnuru Kapu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(327, 3, 'Muthuraja', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(328, 3, 'Naagavamsam', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(329, 3, 'Nadar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(330, 3, 'Nagaralu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(331, 3, 'Nai', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(332, 3, 'Naicken', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(333, 3, 'Naicker', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(334, 3, 'Naidu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(335, 3, 'Naik', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(336, 3, 'Nair', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(337, 3, 'Nair - Vaniya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(338, 3, 'Nair - Velethadathu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(339, 3, 'Nair - Vilakkithala', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(340, 3, 'Namasudra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(341, 3, 'Nambiar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(342, 3, 'Nambisan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(343, 3, 'Namosudra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(344, 3, 'Napit', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(345, 3, 'Nayak', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(346, 3, 'Nayaka', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(347, 3, 'Neeli', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(348, 3, 'Nepali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(349, 3, 'Nhavi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(350, 3, 'OBC - Barber/Naayee', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(351, 3, 'Oswal', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(352, 3, 'Otari', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(353, 3, 'Padmasali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(354, 3, 'Panchal', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(355, 3, 'Pandaram', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(356, 3, 'Panicker', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(357, 3, 'Paravan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(358, 3, 'Parit', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(359, 3, 'Parkava Kulam', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(360, 3, 'Partraj', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(361, 3, 'Patel', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(362, 3, 'Patel - Desai', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(363, 3, 'Patel - Dodia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(364, 3, 'Patel - Kadva', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(365, 3, 'Patel - Leva', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(366, 3, 'Patnaick', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(367, 3, 'Patra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(368, 3, 'Perika', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(369, 3, 'Pillai', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(370, 3, 'Pisharody', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(371, 3, 'Poduval', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(372, 3, 'Poosala', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(373, 3, 'Prajapati', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(374, 3, 'Pulaya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(375, 3, 'Punjabi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(376, 3, 'Rajaka', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(377, 3, 'Rajaka/Chakali/Dhobi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(378, 3, 'Rajbhar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(379, 3, 'Rajput', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(380, 3, 'Rajput - Garhwali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(381, 3, 'Rajput - Kumaoni', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(382, 3, 'Rajput - Lodhi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(383, 3, 'Ramdasia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(384, 3, 'Ramgharia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(385, 3, 'Ravidasia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(386, 3, 'Rawat', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(387, 3, 'Reddiar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(388, 3, 'Reddy', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(389, 3, 'Relli', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(390, 3, 'SK', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(391, 3, 'Sadgop', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(392, 3, 'Sagara - Uppara', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(393, 3, 'Saha', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(394, 3, 'Sahu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(395, 3, 'Saini', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(396, 3, 'Saiva Vellala', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(397, 3, 'Saliya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(398, 3, 'Sambava', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(399, 3, 'Savji', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(400, 3, 'Scheduled Caste (SC)', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(401, 3, 'Scheduled Tribe (ST)', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(402, 3, 'Senai Thalaivar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(403, 3, 'Sepahia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(404, 3, 'Setti Balija', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(405, 3, 'Shah', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(406, 3, 'Shimpi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(407, 3, 'Sindhi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(408, 3, 'Sindhi - Bhanusali', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(409, 3, 'Sindhi - Bhatia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(410, 3, 'Sindhi - Chhapru', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(411, 3, 'Sindhi - Dadu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(412, 3, 'Sindhi - Hyderabadi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(413, 3, 'Sindhi - Larai', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(414, 3, 'Sindhi - Lohana', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(415, 3, 'Sindhi - Rohiri', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(416, 3, 'Sindhi - Sehwani', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(417, 3, 'Sindhi - Thatai', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(418, 3, 'Sindhi-Amil', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(419, 3, 'Sindhi-Baibhand', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(420, 3, 'Sindhi-Larkana', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(421, 3, 'Sindhi-Sahiti', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(422, 3, 'Sindhi-Sakkhar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(423, 3, 'Sindhi-Shikarpuri', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(424, 3, 'Somvanshi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(425, 3, 'Sonar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(426, 3, 'Soni', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(427, 3, 'Sourashtra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(428, 3, 'Sowrashtra', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(429, 3, 'Sozhiya Vellalar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(430, 3, 'Sri Vaishnava', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(431, 3, 'Srisayana', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(432, 3, 'Subarna Banik', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(433, 3, 'Sugali (Naika)', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(434, 3, 'Sundhi', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(435, 3, 'Surya Balija', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(436, 3, 'Sutar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(437, 3, 'Swarnakar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(438, 3, 'Tamboli', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(439, 3, 'Tamil Yadava', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(440, 3, 'Tanti', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(441, 3, 'Tantuway', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(442, 3, 'Telaga', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(443, 3, 'Teli', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(444, 3, 'Telugu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(445, 3, 'Thachar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(446, 3, 'Thakkar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(447, 3, 'Thakur', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(448, 3, 'Thandan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(449, 3, 'Thigala', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(450, 3, 'Thiyya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(451, 3, 'Togata', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(452, 3, 'Turupu Kapu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(453, 3, 'Udayar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(454, 3, 'Urali Gounder', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(455, 3, 'Urs', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(456, 3, 'Vada Balija', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(457, 3, 'Vadagalai', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(458, 3, 'Vaddera', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(459, 3, 'Vaduka', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(460, 3, 'Vaish', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(461, 3, 'Vaish - Dhaneshawat', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(462, 3, 'Vaishnav', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(463, 3, 'Vaishnav - Bhatia', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(464, 3, 'Vaishnav - Vania', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(465, 3, 'Vaishya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(466, 3, 'Vallala', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(467, 3, 'Valluvan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(468, 3, 'Valmiki', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(469, 3, 'Vanika Vyshya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(470, 3, 'Vanjara', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(471, 3, 'Vankar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(472, 3, 'Vannan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(473, 3, 'Vannar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(474, 3, 'Vanniyakullak Kshatriya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(475, 3, 'Vanniyar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(476, 3, 'Variar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(477, 3, 'Varshney', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(478, 3, 'Veera Saivam', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(479, 3, 'Veerashaiva', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(480, 3, 'Velaan', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(481, 3, 'Velama', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(482, 3, 'Velar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(483, 3, 'Vellalar', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(484, 3, 'Veluthedathu - Nair', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(485, 3, 'Vettuva Gounder', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(486, 3, 'Vishwakarma', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(487, 3, 'Viswabrahmin', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(488, 3, 'Vokaliga', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(489, 3, 'Vokkaliga', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(490, 3, 'Vysya', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(491, 3, 'Waada Balija', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(492, 3, 'Yellapu', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(493, 3, 'Yadav', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(494, 3, 'Others', 1, 1, '2019-09-19 13:29:20', NULL, NULL),
(495, 2, 'Bengali', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(496, 2, 'Dawoodi Bohra', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(497, 2, 'Khoja', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(498, 2, 'Mapila', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(499, 2, 'Memon', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(500, 2, 'Rajput', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(501, 2, 'Rowther', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(502, 2, 'Shafi', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(503, 2, 'Shia', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(504, 2, 'Shia Bohra', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(505, 2, 'Shia Imami Ismaili', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(506, 2, 'Shia Ithna ashariyyah', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(507, 2, 'Shia Zaidi', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(508, 2, 'Sunni', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(509, 2, 'Sunni Ehle-Hadith', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(510, 2, 'Sunni Hanafi', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(511, 2, 'Sunni Hunbali', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(512, 2, 'Sunni Maliki', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(513, 2, 'Sunni Shafi', 1, 1, '2019-09-19 13:34:34', NULL, NULL),
(514, 4, 'Ahluwalia', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(515, 4, 'Arora', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(516, 4, 'Clean Shaven', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(517, 4, 'Gursikh', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(518, 4, 'Jat', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(519, 4, 'Kamboj', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(520, 4, 'Kesadhari', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(521, 4, 'Khatri', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(522, 4, 'Kshatriya', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(523, 4, 'Labana', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(524, 4, 'Mazhbi/Majabi', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(525, 4, 'Rajput', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(526, 4, 'Ramdasia', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(527, 4, 'Ramgharia', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(528, 4, 'Ravidasia', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(529, 4, 'Saini', 1, 1, '2019-09-19 14:52:18', NULL, NULL),
(530, 6, 'Jain Caste', 1, 1, '2019-09-19 14:57:50', NULL, NULL),
(531, 6, 'Digambar', 1, 1, '2019-09-19 14:57:50', NULL, NULL),
(532, 6, 'Shwetamber', 1, 1, '2019-09-19 14:57:50', NULL, NULL),
(533, 6, 'Vania', 1, 1, '2019-09-19 14:57:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_complexion`
--

CREATE TABLE `mt_complexion` (
  `cplx_id` int(11) NOT NULL,
  `cplx_type` varchar(100) NOT NULL,
  `cplx_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `cplx_cr_uid` int(11) NOT NULL,
  `cplx_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cplx_up_uid` int(11) DEFAULT NULL,
  `cplx_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_complexion`
--

INSERT INTO `mt_complexion` (`cplx_id`, `cplx_type`, `cplx_is_frz`, `cplx_cr_uid`, `cplx_cr_dt`, `cplx_up_uid`, `cplx_up_dt`) VALUES
(1, 'Very Fair ', 1, 1, '2019-09-18 16:30:23', NULL, NULL),
(2, 'Fair', 1, 1, '2019-09-18 16:30:23', NULL, NULL),
(3, 'Wheatish ', 1, 1, '2019-09-18 16:30:23', NULL, NULL),
(4, 'Wheatish Brown', 1, 1, '2019-09-18 16:30:23', NULL, NULL),
(5, 'Dark', 1, 1, '2019-09-18 16:30:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_currency`
--

CREATE TABLE `mt_currency` (
  `curr_id` int(11) NOT NULL,
  `curr_type` varchar(100) NOT NULL,
  `curr_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `curr_cr_uid` int(11) NOT NULL,
  `curr_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `curr_up_uid` int(11) DEFAULT NULL,
  `curr_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_dosam`
--

CREATE TABLE `mt_dosam` (
  `dosam_id` int(11) NOT NULL,
  `dosam_type` varchar(100) NOT NULL,
  `dosam_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `dosam_cr_uid` int(11) NOT NULL,
  `dosam_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dosam_up_uid` int(11) DEFAULT NULL,
  `dosam_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_dosam`
--

INSERT INTO `mt_dosam` (`dosam_id`, `dosam_type`, `dosam_is_frz`, `dosam_cr_uid`, `dosam_cr_dt`, `dosam_up_uid`, `dosam_up_dt`) VALUES
(1, 'Yes', 1, 1, '2019-09-18 17:12:09', NULL, NULL),
(2, 'No', 1, 1, '2019-09-18 17:12:09', NULL, NULL),
(3, 'Don’t Know', 1, 1, '2019-09-18 17:12:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_drinking_habit`
--

CREATE TABLE `mt_drinking_habit` (
  `dh_id` int(11) NOT NULL,
  `dh_type` varchar(100) NOT NULL,
  `dh_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `dh_cr_uid` int(11) NOT NULL,
  `dh_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dh_up_uid` int(11) DEFAULT NULL,
  `dh_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_drinking_habit`
--

INSERT INTO `mt_drinking_habit` (`dh_id`, `dh_type`, `dh_is_frz`, `dh_cr_uid`, `dh_cr_dt`, `dh_up_uid`, `dh_up_dt`) VALUES
(1, 'Yes', 1, 1, '2019-09-18 16:41:30', NULL, NULL),
(2, 'No', 1, 1, '2019-09-18 16:41:30', NULL, NULL),
(3, 'Drinks Socially', 1, 1, '2019-09-18 16:41:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_eating_habit`
--

CREATE TABLE `mt_eating_habit` (
  `eh_id` int(11) NOT NULL,
  `eh_type` varchar(100) NOT NULL,
  `eh_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `eh_cr_uid` int(11) NOT NULL,
  `eh_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `eh_up_uid` int(11) DEFAULT NULL,
  `eh_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_eating_habit`
--

INSERT INTO `mt_eating_habit` (`eh_id`, `eh_type`, `eh_is_frz`, `eh_cr_uid`, `eh_cr_dt`, `eh_up_uid`, `eh_up_dt`) VALUES
(1, 'Vegetarian', 1, 1, '2019-09-18 16:40:13', NULL, NULL),
(2, 'Non Vegetarian', 1, 1, '2019-09-18 16:40:13', NULL, NULL),
(3, 'Eggetarian', 1, 1, '2019-09-18 16:40:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_edu`
--

CREATE TABLE `mt_edu` (
  `edu_id` int(11) NOT NULL,
  `edu_course` varchar(250) NOT NULL,
  `edu_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `edu_cr_uid` int(11) NOT NULL,
  `edu_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edu_up_uid` int(11) DEFAULT NULL,
  `edu_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_emp_type`
--

CREATE TABLE `mt_emp_type` (
  `emp_type_id` int(11) NOT NULL,
  `emp_type_type` varchar(100) NOT NULL,
  `emp_type_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `emp_type_cr_uid` int(11) NOT NULL,
  `emp_type_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `emp_type_up_uid` int(11) DEFAULT NULL,
  `emp_type_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_emp_type`
--

INSERT INTO `mt_emp_type` (`emp_type_id`, `emp_type_type`, `emp_type_is_frz`, `emp_type_cr_uid`, `emp_type_cr_dt`, `emp_type_up_uid`, `emp_type_up_dt`) VALUES
(1, 'Private Company', 1, 1, '2019-09-18 17:23:58', NULL, NULL),
(2, 'Government / Public Sector', 1, 1, '2019-09-18 17:23:58', NULL, NULL),
(3, 'Defense / Civil Services', 1, 1, '2019-09-18 17:23:58', NULL, NULL),
(4, 'Business / Self Employed', 1, 1, '2019-09-18 17:23:58', NULL, NULL),
(5, 'Non Working', 1, 1, '2019-09-18 17:23:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_family_location`
--

CREATE TABLE `mt_family_location` (
  `f_loc_id` int(11) NOT NULL,
  `f_loc_country_id` int(11) NOT NULL,
  `f_loc_state_id` int(11) NOT NULL,
  `f_loc_city_id` int(11) NOT NULL,
  `f_loc_cr_uid` int(11) NOT NULL,
  `f_loc_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_loc_up_uid` int(11) DEFAULT NULL,
  `f_loc_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_family_status`
--

CREATE TABLE `mt_family_status` (
  `fs_id` int(11) NOT NULL,
  `fs_type` varchar(100) NOT NULL,
  `fs_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `fs_cr_uid` int(11) NOT NULL,
  `fs_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fs_up_uid` int(11) DEFAULT NULL,
  `fs_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_family_status`
--

INSERT INTO `mt_family_status` (`fs_id`, `fs_type`, `fs_is_frz`, `fs_cr_uid`, `fs_cr_dt`, `fs_up_uid`, `fs_up_dt`) VALUES
(1, 'Middle Class', 1, 1, '2019-09-18 17:28:54', NULL, NULL),
(2, 'Upper Middle Class', 1, 1, '2019-09-18 17:28:54', NULL, NULL),
(3, 'Rich', 1, 1, '2019-09-18 17:28:54', NULL, NULL),
(4, 'Affluent', 1, 1, '2019-09-18 17:28:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_family_type`
--

CREATE TABLE `mt_family_type` (
  `ft_id` int(11) NOT NULL,
  `ft_type` varchar(100) NOT NULL,
  `ft_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `ft_cr_uid` int(11) NOT NULL,
  `ft_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ft_up_uid` int(11) DEFAULT NULL,
  `ft_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_family_type`
--

INSERT INTO `mt_family_type` (`ft_id`, `ft_type`, `ft_is_frz`, `ft_cr_uid`, `ft_cr_dt`, `ft_up_uid`, `ft_up_dt`) VALUES
(1, 'Joint Family', 1, 1, '2019-09-18 17:27:23', NULL, NULL),
(2, 'Nuclear Family', 1, 1, '2019-09-18 17:27:23', NULL, NULL),
(3, 'Others', 1, 1, '2019-09-18 17:27:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_family_value`
--

CREATE TABLE `mt_family_value` (
  `fv_id` int(11) NOT NULL,
  `fv_type` varchar(100) NOT NULL,
  `fv_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `fv_cr_uid` int(11) NOT NULL,
  `fv_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fv_up_uid` int(11) DEFAULT NULL,
  `fv_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_family_value`
--

INSERT INTO `mt_family_value` (`fv_id`, `fv_type`, `fv_is_frz`, `fv_cr_uid`, `fv_cr_dt`, `fv_up_uid`, `fv_up_dt`) VALUES
(1, 'Orthodox', 1, 1, '2019-09-18 17:26:17', NULL, NULL),
(2, 'Traditional', 1, 1, '2019-09-18 17:26:17', NULL, NULL),
(3, 'Moderate', 1, 1, '2019-09-18 17:26:17', NULL, NULL),
(4, 'Liberal', 1, 1, '2019-09-18 17:26:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_father_status`
--

CREATE TABLE `mt_father_status` (
  `fas_id` int(11) NOT NULL,
  `fas_type` varchar(100) NOT NULL,
  `fas_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `fas_cr_uid` int(11) NOT NULL,
  `fas_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fas_up_uid` int(11) DEFAULT NULL,
  `fas_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_father_status`
--

INSERT INTO `mt_father_status` (`fas_id`, `fas_type`, `fas_is_frz`, `fas_cr_uid`, `fas_cr_dt`, `fas_up_uid`, `fas_up_dt`) VALUES
(1, 'Employed', 1, 1, '2019-09-18 17:30:34', NULL, NULL),
(2, 'Business Man', 1, 1, '2019-09-18 17:30:34', NULL, NULL),
(3, 'Professional', 1, 1, '2019-09-18 17:30:34', NULL, NULL),
(4, 'Retired', 1, 1, '2019-09-18 17:30:34', NULL, NULL),
(5, 'Not Employed', 1, 1, '2019-09-18 17:30:34', NULL, NULL),
(6, 'Passed Away', 1, 1, '2019-09-18 17:30:34', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_fav_music`
--

CREATE TABLE `mt_fav_music` (
  `fav_mu_id` int(11) NOT NULL,
  `fav_mu_type` varchar(100) NOT NULL,
  `fav_mu_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `fav_mu_cr_uid` int(11) NOT NULL,
  `fav_mu_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fav_mu_up_uid` int(11) DEFAULT NULL,
  `fav_mu_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_fav_music`
--

INSERT INTO `mt_fav_music` (`fav_mu_id`, `fav_mu_type`, `fav_mu_is_frz`, `fav_mu_cr_uid`, `fav_mu_cr_dt`, `fav_mu_up_uid`, `fav_mu_up_dt`) VALUES
(1, 'Film songs', 1, 1, '2019-09-18 17:47:38', NULL, NULL),
(2, 'Indian / Classical Music', 1, 1, '2019-09-18 17:47:38', NULL, NULL),
(3, 'Western Music', 1, 1, '2019-09-18 17:47:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_hobbies_interest`
--

CREATE TABLE `mt_hobbies_interest` (
  `hobbies_id` int(11) NOT NULL,
  `hobbies_type` varchar(100) NOT NULL,
  `hobbies_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `hobbies_cr_uid` int(11) NOT NULL,
  `hobbies_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hobbies_up_uid` int(11) DEFAULT NULL,
  `hobbies_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_hobbies_interest`
--

INSERT INTO `mt_hobbies_interest` (`hobbies_id`, `hobbies_type`, `hobbies_is_frz`, `hobbies_cr_uid`, `hobbies_cr_dt`, `hobbies_up_uid`, `hobbies_up_dt`) VALUES
(1, 'Cooking', 1, 1, '2019-09-18 17:46:36', NULL, NULL),
(2, 'Nature', 1, 1, '2019-09-18 17:46:36', NULL, NULL),
(3, 'Photography', 1, 1, '2019-09-18 17:46:36', NULL, NULL),
(4, 'Dancing', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(5, 'Painting', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(6, 'Pets', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(7, 'Playing Musical Instruments', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(8, 'Gardening / Landscaping', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(9, 'Art / Handicraft', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(10, 'Listening to Music', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(11, 'Movies', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(12, 'Internet Surfing', 1, 1, '2019-09-18 17:46:37', NULL, NULL),
(13, 'Traveling', 1, 1, '2019-09-18 17:46:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_language`
--

CREATE TABLE `mt_language` (
  `lan_id` int(11) NOT NULL,
  `lan_type` varchar(100) NOT NULL,
  `lan_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `lan_cr_uid` int(11) NOT NULL,
  `lan_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lan_up_uid` int(11) DEFAULT NULL,
  `lan_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_language`
--

INSERT INTO `mt_language` (`lan_id`, `lan_type`, `lan_is_frz`, `lan_cr_uid`, `lan_cr_dt`, `lan_up_uid`, `lan_up_dt`) VALUES
(1, 'Tamil', 1, 1, '2019-09-18 16:38:50', NULL, NULL),
(2, 'Bengali', 1, 1, '2019-09-18 16:38:50', NULL, NULL),
(3, 'Hindi', 1, 1, '2019-09-18 16:38:50', NULL, NULL),
(4, 'Marathi', 1, 1, '2019-09-18 16:38:50', NULL, NULL),
(5, 'Telugu', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(6, 'Gujarati', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(7, 'Urdu', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(8, 'Kannada', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(9, 'Odia', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(10, 'Malayalam', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(11, 'Punjabi', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(12, 'Assamese', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(13, 'Maithili', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(14, 'Bhili/Bhilodi', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(15, 'Santali', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(16, 'Kashmiri', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(17, 'Gondi', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(18, 'Nepali', 1, 1, '2019-09-18 16:38:51', NULL, NULL),
(19, 'Sindhi', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(20, 'Dogri', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(21, 'Konkani', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(22, 'Kurukh', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(23, 'Khandeshi', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(24, 'Tulu', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(25, 'Gondi', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(26, 'Manipuri', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(27, 'Bodo', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(28, 'Khasi', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(29, 'Ho', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(30, 'Mundari', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(31, 'Garo', 1, 1, '2019-09-18 16:38:52', NULL, NULL),
(32, 'Tripuri', 1, 1, '2019-09-18 16:38:52', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_marital_status`
--

CREATE TABLE `mt_marital_status` (
  `ms_id` int(11) NOT NULL,
  `ms_type` varchar(100) NOT NULL,
  `ms_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `ms_cr_uid` int(11) NOT NULL,
  `ms_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ms_up_uid` int(11) DEFAULT NULL,
  `ms_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_marital_status`
--

INSERT INTO `mt_marital_status` (`ms_id`, `ms_type`, `ms_is_frz`, `ms_cr_uid`, `ms_cr_dt`, `ms_up_uid`, `ms_up_dt`) VALUES
(1, 'Never Married', 1, 1, '2019-09-18 16:25:40', NULL, NULL),
(2, 'Widowed', 1, 1, '2019-09-18 16:25:40', NULL, NULL),
(3, 'Divorced', 1, 1, '2019-09-18 16:25:40', NULL, NULL),
(4, 'Awaiting Divorce', 1, 1, '2019-09-18 16:25:40', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_mother_status`
--

CREATE TABLE `mt_mother_status` (
  `mos_id` int(11) NOT NULL,
  `mos_type` varchar(100) NOT NULL,
  `mos_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `mos_cr_uid` int(11) NOT NULL,
  `mos_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mos_up_uid` int(11) DEFAULT NULL,
  `mos_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_mother_status`
--

INSERT INTO `mt_mother_status` (`mos_id`, `mos_type`, `mos_is_frz`, `mos_cr_uid`, `mos_cr_dt`, `mos_up_uid`, `mos_up_dt`) VALUES
(1, 'Employed', 1, 1, '2019-09-18 17:31:39', NULL, NULL),
(2, 'Business Women', 1, 1, '2019-09-18 17:31:39', NULL, NULL),
(3, 'Professional', 1, 1, '2019-09-18 17:31:39', NULL, NULL),
(4, 'Retired', 1, 1, '2019-09-18 17:31:39', NULL, NULL),
(5, 'Not Employed', 1, 1, '2019-09-18 17:31:39', NULL, NULL),
(6, 'Passed Away', 1, 1, '2019-09-18 17:31:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_occupation`
--

CREATE TABLE `mt_occupation` (
  `occ_id` int(11) NOT NULL,
  `occ_type` varchar(250) NOT NULL,
  `occ_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `occ_cr_uid` int(11) NOT NULL,
  `occ_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `occ_up_uid` int(11) DEFAULT NULL,
  `occ_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_physical_status`
--

CREATE TABLE `mt_physical_status` (
  `ps_id` int(11) NOT NULL,
  `ps_type` varchar(100) NOT NULL,
  `ps_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `ps_cr_uid` int(11) NOT NULL,
  `ps_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ps_up_uid` int(11) DEFAULT NULL,
  `ps_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_physical_status`
--

INSERT INTO `mt_physical_status` (`ps_id`, `ps_type`, `ps_is_frz`, `ps_cr_uid`, `ps_cr_dt`, `ps_up_uid`, `ps_up_dt`) VALUES
(1, 'Normal', 1, 1, '2019-09-18 16:31:51', NULL, NULL),
(2, 'Physically Challenged', 1, 1, '2019-09-18 16:31:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_premium_package`
--

CREATE TABLE `mt_premium_package` (
  `pp_id` int(11) NOT NULL,
  `pp_desc` text NOT NULL,
  `pp_image_path` varchar(250) NOT NULL,
  `pp_cost` double NOT NULL,
  `pp_discount` double NOT NULL,
  `pp_days` int(11) NOT NULL,
  `pp_mobile_no_count` int(11) NOT NULL,
  `pp_is_frz` int(11) NOT NULL DEFAULT '1',
  `pp_cr_uid` int(11) NOT NULL,
  `pp_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pp_up_uid` int(11) DEFAULT NULL,
  `pp_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_profile`
--

CREATE TABLE `mt_profile` (
  `p_id` int(11) NOT NULL,
  `p_pro_cre_id` int(11) NOT NULL,
  `p_name` varchar(250) NOT NULL,
  `p_dob` date NOT NULL,
  `p_gender` tinyint(1) NOT NULL,
  `p_height` double NOT NULL,
  `p_weight` double NOT NULL,
  `p_mob_no1` varchar(15) NOT NULL,
  `p_mon_no2` varchar(15) DEFAULT NULL,
  `p_email` varchar(100) NOT NULL,
  `p_password` text NOT NULL,
  `p_marital_status_id` int(11) NOT NULL,
  `p_body_type_id` int(11) DEFAULT NULL,
  `p_complexion_id` int(11) DEFAULT NULL,
  `p_physical_status_id` int(11) DEFAULT '1',
  `p_mother_tongue_id` int(11) NOT NULL,
  `p_lan_can_read` tinyint(1) DEFAULT '1',
  `p_eh_ids` text,
  `p_dh_ids` text,
  `p_sh_ids` text,
  `p_about_me` text,
  `p_about_family` text,
  `p_abt_exp_looking_partner` text,
  `p_otp` varchar(10) DEFAULT NULL,
  `p_is_verified` tinyint(1) DEFAULT '2',
  `p_last_login_dt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `p_is_paid_user` tinyint(1) DEFAULT '2',
  `p_pre_package_id` int(11) DEFAULT NULL,
  `p_cr_dt` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_profile_creator`
--

CREATE TABLE `mt_profile_creator` (
  `profile_creator_id` int(11) NOT NULL,
  `profile_creator_type` varchar(100) NOT NULL,
  `profile_creator_is_frz` tinyint(1) DEFAULT '1',
  `profile_creator_cr_uid` int(11) NOT NULL,
  `profile_creator_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `profile_creator_up_uid` int(11) DEFAULT NULL,
  `profile_creator_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_profile_creator`
--

INSERT INTO `mt_profile_creator` (`profile_creator_id`, `profile_creator_type`, `profile_creator_is_frz`, `profile_creator_cr_uid`, `profile_creator_cr_dt`, `profile_creator_up_uid`, `profile_creator_up_dt`) VALUES
(1, 'MySelf', 1, 1, '2019-09-18 16:22:13', NULL, NULL),
(2, 'Relative', 1, 1, '2019-09-18 16:23:02', NULL, NULL),
(3, 'Friend', 1, 1, '2019-09-18 16:23:02', NULL, NULL),
(4, 'Son', 1, 1, '2019-09-18 16:23:02', NULL, NULL),
(5, 'Brother', 1, 1, '2019-09-18 16:23:03', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_rasi_moon_sign`
--

CREATE TABLE `mt_rasi_moon_sign` (
  `rms_id` int(11) NOT NULL,
  `rms_star_id` int(11) NOT NULL,
  `rms_zodiac_id` int(11) NOT NULL,
  `rms_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `rms_cr_uid` int(11) NOT NULL,
  `rms_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rms_up_uid` int(11) DEFAULT NULL,
  `rms_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_rasi_moon_sign`
--

INSERT INTO `mt_rasi_moon_sign` (`rms_id`, `rms_star_id`, `rms_zodiac_id`, `rms_is_frz`, `rms_cr_uid`, `rms_cr_dt`, `rms_up_uid`, `rms_up_dt`) VALUES
(1, 1, 1, 1, 1, '2019-09-19 10:21:48', NULL, NULL),
(2, 2, 1, 1, 1, '2019-09-19 10:21:48', NULL, NULL),
(3, 3, 1, 1, 1, '2019-09-19 10:21:48', NULL, NULL),
(4, 3, 2, 1, 1, '2019-09-19 10:21:48', NULL, NULL),
(5, 4, 2, 1, 1, '2019-09-19 10:40:56', NULL, NULL),
(6, 5, 2, 1, 1, '2019-09-19 10:53:40', NULL, NULL),
(7, 5, 3, 1, 1, '2019-09-19 10:53:40', NULL, NULL),
(8, 6, 3, 1, 1, '2019-09-19 10:54:51', NULL, NULL),
(9, 7, 3, 1, 1, '2019-09-19 11:00:20', NULL, NULL),
(10, 7, 4, 1, 1, '2019-09-19 11:00:20', NULL, NULL),
(11, 8, 4, 1, 1, '2019-09-19 11:00:59', NULL, NULL),
(12, 9, 5, 1, 1, '2019-09-19 11:04:00', NULL, NULL),
(13, 10, 5, 1, 1, '2019-09-19 11:09:47', NULL, NULL),
(14, 11, 5, 1, 1, '2019-09-19 11:12:46', NULL, NULL),
(15, 12, 5, 1, 1, '2019-09-19 11:14:12', NULL, NULL),
(16, 12, 6, 1, 1, '2019-09-19 11:14:12', NULL, NULL),
(17, 13, 6, 1, 1, '2019-09-19 11:15:00', NULL, NULL),
(18, 14, 6, 1, 1, '2019-09-19 11:16:13', NULL, NULL),
(19, 14, 7, 1, 1, '2019-09-19 11:16:13', NULL, NULL),
(20, 15, 7, 1, 1, '2019-09-19 11:17:17', NULL, NULL),
(21, 16, 7, 1, 1, '2019-09-19 11:19:39', NULL, NULL),
(22, 16, 8, 1, 1, '2019-09-19 11:19:39', NULL, NULL),
(23, 17, 8, 1, 1, '2019-09-19 11:21:05', NULL, NULL),
(24, 18, 8, 1, 1, '2019-09-19 11:22:09', NULL, NULL),
(25, 19, 9, 1, 1, '2019-09-19 11:23:25', NULL, NULL),
(26, 20, 9, 1, 1, '2019-09-19 11:24:16', NULL, NULL),
(27, 21, 9, 1, 1, '2019-09-19 11:25:05', NULL, NULL),
(28, 21, 10, 1, 1, '2019-09-19 11:25:05', NULL, NULL),
(29, 22, 10, 1, 1, '2019-09-19 11:26:38', NULL, NULL),
(30, 23, 10, 1, 1, '2019-09-19 11:27:49', NULL, NULL),
(31, 23, 11, 1, 1, '2019-09-19 11:27:49', NULL, NULL),
(32, 24, 11, 1, 1, '2019-09-19 11:28:53', NULL, NULL),
(33, 25, 11, 1, 1, '2019-09-19 11:32:01', NULL, NULL),
(34, 25, 12, 1, 1, '2019-09-19 11:32:01', NULL, NULL),
(35, 26, 12, 1, 1, '2019-09-19 11:33:33', NULL, NULL),
(36, 27, 12, 1, 1, '2019-09-19 11:34:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_religion`
--

CREATE TABLE `mt_religion` (
  `religion_id` int(11) NOT NULL,
  `religion_type` varchar(100) NOT NULL,
  `religion_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `religion_cr_uid` int(11) NOT NULL,
  `religion_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `religion_up_uid` int(11) DEFAULT NULL,
  `religion_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_religion`
--

INSERT INTO `mt_religion` (`religion_id`, `religion_type`, `religion_is_frz`, `religion_cr_uid`, `religion_cr_dt`, `religion_up_uid`, `religion_up_dt`) VALUES
(1, 'Christian', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(2, 'Muslim', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(3, 'Hindu', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(4, 'Sikh', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(5, 'Parsi', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(6, 'Jain', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(7, 'Buddhist', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(8, 'Jewish', 1, 1, '2019-09-18 16:53:24', NULL, NULL),
(9, 'Other', 1, 1, '2019-09-18 16:53:24', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_resident_status`
--

CREATE TABLE `mt_resident_status` (
  `rs_id` int(11) NOT NULL,
  `rs_type` varchar(100) NOT NULL,
  `rs_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `rs_cr_uid` int(11) NOT NULL,
  `rs_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rs_up_uid` int(11) DEFAULT NULL,
  `rs_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_resident_status`
--

INSERT INTO `mt_resident_status` (`rs_id`, `rs_type`, `rs_is_frz`, `rs_cr_uid`, `rs_cr_dt`, `rs_up_uid`, `rs_up_dt`) VALUES
(1, 'Permanent Resident', 1, 1, '2019-09-18 17:18:18', NULL, NULL),
(2, 'Work Permit', 1, 1, '2019-09-18 17:18:18', NULL, NULL),
(3, 'Student Visa', 1, 1, '2019-09-18 17:18:18', NULL, NULL),
(4, 'Temporary Visa', 1, 1, '2019-09-18 17:18:18', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_smoking_habit`
--

CREATE TABLE `mt_smoking_habit` (
  `sh_id` int(11) NOT NULL,
  `sh_type` varchar(100) NOT NULL,
  `sh_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `sh_cr_uid` int(11) NOT NULL,
  `sh_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sh_up_uid` int(11) DEFAULT NULL,
  `sh_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_smoking_habit`
--

INSERT INTO `mt_smoking_habit` (`sh_id`, `sh_type`, `sh_is_frz`, `sh_cr_uid`, `sh_cr_dt`, `sh_up_uid`, `sh_up_dt`) VALUES
(1, 'Yes', 1, 1, '2019-09-18 16:42:32', NULL, NULL),
(2, 'No', 1, 1, '2019-09-18 16:42:32', NULL, NULL),
(3, 'Occasionally', 1, 1, '2019-09-18 16:42:32', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_sports_fitness`
--

CREATE TABLE `mt_sports_fitness` (
  `sp_ft_id` int(11) NOT NULL,
  `sp_ft_type` varchar(100) NOT NULL,
  `sp_ft_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `sp_ft_cr_uid` int(11) NOT NULL,
  `sp_ft_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sp_ft_up_uid` int(11) DEFAULT NULL,
  `sp_ft_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_sports_fitness`
--

INSERT INTO `mt_sports_fitness` (`sp_ft_id`, `sp_ft_type`, `sp_ft_is_frz`, `sp_ft_cr_uid`, `sp_ft_cr_dt`, `sp_ft_up_uid`, `sp_ft_up_dt`) VALUES
(1, 'Cricket', 1, 1, '2019-09-18 17:49:43', NULL, NULL),
(2, 'Carrom', 1, 1, '2019-09-18 17:49:43', NULL, NULL),
(3, 'Chess', 1, 1, '2019-09-18 17:49:43', NULL, NULL),
(4, 'Jogging', 1, 1, '2019-09-18 17:49:43', NULL, NULL),
(5, 'Badminton', 1, 1, '2019-09-18 17:49:44', NULL, NULL),
(6, 'Swimming', 1, 1, '2019-09-18 17:49:44', NULL, NULL),
(7, 'Tennis', 1, 1, '2019-09-18 17:49:44', NULL, NULL),
(8, 'FootballOthers', 1, 1, '2019-09-18 17:49:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_star`
--

CREATE TABLE `mt_star` (
  `star_id` int(11) NOT NULL,
  `star_type` varchar(100) NOT NULL,
  `star_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `star_cr_uid` int(11) NOT NULL,
  `star_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `star_up_uid` int(11) DEFAULT NULL,
  `star_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_star`
--

INSERT INTO `mt_star` (`star_id`, `star_type`, `star_is_frz`, `star_cr_uid`, `star_cr_dt`, `star_up_uid`, `star_up_dt`) VALUES
(1, 'Ashwini', 1, 1, '2019-09-19 09:29:48', NULL, NULL),
(2, 'Bharani', 1, 1, '2019-09-19 09:29:48', NULL, NULL),
(3, 'Karthigai', 1, 1, '2019-09-19 09:29:48', NULL, NULL),
(4, 'Rohini', 1, 1, '2019-09-19 09:29:48', NULL, NULL),
(5, 'Mirigasirisham', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(6, 'Thiruvathirai', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(7, 'Punarpoosam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(8, 'Poosam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(9, 'Ayilyam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(10, 'Makam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(11, 'Pooram', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(12, 'Uthiram', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(13, 'Hastham', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(14, 'Chithirai', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(15, 'Swathi', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(16, 'Visakam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(17, 'Anusham', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(18, 'Kettai', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(19, 'Moolam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(20, 'Pooradam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(21, 'Uthradam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(22, 'Thiruvonam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(23, 'Avittam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(24, 'Sadhayam', 1, 1, '2019-09-19 09:29:49', NULL, NULL),
(25, 'Puratathi', 1, 1, '2019-09-19 09:29:50', NULL, NULL),
(26, 'Uthirattathi', 1, 1, '2019-09-19 09:29:50', NULL, NULL),
(27, 'Revathi', 1, 1, '2019-09-19 09:29:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mt_sub_caste`
--

CREATE TABLE `mt_sub_caste` (
  `sub_caste_id` int(11) NOT NULL,
  `caste_id` int(11) NOT NULL,
  `sub_caste_type` varchar(100) NOT NULL,
  `sub_caste_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `sub_caste_cr_uid` int(11) NOT NULL,
  `sub_caste_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sub_caste_up_uid` int(11) DEFAULT NULL,
  `sub_caste_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mt_zodiac_star_sign`
--

CREATE TABLE `mt_zodiac_star_sign` (
  `zodiac_id` int(11) NOT NULL,
  `zodiac_type` varchar(100) NOT NULL,
  `zodiac_tamil` varchar(100) DEFAULT NULL,
  `zodiac_is_frz` tinyint(1) NOT NULL DEFAULT '1',
  `zodiac_cr_uid` int(11) NOT NULL,
  `zodiac_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `zodiac_up_uid` int(11) DEFAULT NULL,
  `zodiac_up_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mt_zodiac_star_sign`
--

INSERT INTO `mt_zodiac_star_sign` (`zodiac_id`, `zodiac_type`, `zodiac_tamil`, `zodiac_is_frz`, `zodiac_cr_uid`, `zodiac_cr_dt`, `zodiac_up_uid`, `zodiac_up_dt`) VALUES
(1, 'Aries', 'Mesham', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(2, 'Taurus', 'Rishabam', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(3, 'Gemini', 'Mithunam', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(4, 'Cancer', 'Katagam', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(5, 'Leo', 'Simham', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(6, 'Virgo', 'Kanni', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(7, 'Libra', 'Tula', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(8, 'Scorpio', 'Vrichigam', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(9, 'Sagittarius', 'Dhanus', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(10, 'Capricorn', 'Makara', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(11, 'Aquarius', 'Kumbha', 1, 1, '2019-09-19 09:37:54', NULL, NULL),
(12, 'Pisces', 'Meena', 1, 1, '2019-09-19 09:37:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `st_profile`
--

CREATE TABLE `st_profile` (
  `sett_id` int(11) NOT NULL,
  `sett_profile_id` varchar(45) NOT NULL,
  `sett_auto_email` tinyint(1) NOT NULL DEFAULT '1',
  `sett_profile_photo_is_hidden` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_mobile_no_viewed`
--

CREATE TABLE `tt_mobile_no_viewed` (
  `mv_id` int(11) NOT NULL,
  `mv_profile_id` int(11) NOT NULL,
  `mv_person_id` int(11) NOT NULL,
  `mv_dt` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_premium_members`
--

CREATE TABLE `tt_premium_members` (
  `pm_id` int(11) NOT NULL,
  `pm_profile_id` int(11) NOT NULL,
  `pm_pp_id` int(11) NOT NULL,
  `pm_pp_cost` double NOT NULL,
  `pm_pp_discount` double NOT NULL,
  `pm_pp_days` int(11) NOT NULL,
  `pm_pp_carryover_ref_id` int(11) DEFAULT NULL,
  `pm_pp_carryover_mobile_no_count` int(11) DEFAULT NULL,
  `pm_pp_mobile_no_count` int(11) NOT NULL,
  `pm_pp_bal_mobile_no_count` int(11) NOT NULL,
  `pm_pp_start_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pm_pp_end_dt` datetime NOT NULL,
  `pm_pp_telecaller_id` int(11) DEFAULT NULL,
  `pm_pp_cr_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_pro_ig_by_me`
--

CREATE TABLE `tt_pro_ig_by_me` (
  `pig_id` int(11) NOT NULL,
  `pig_profile_id` int(11) NOT NULL,
  `pig_person_id` int(11) NOT NULL,
  `pig_dt` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_request`
--

CREATE TABLE `tt_request` (
  `req_id` int(11) NOT NULL,
  `req_profile_id` int(11) NOT NULL,
  `req_person_id` int(11) NOT NULL,
  `req_is_accepted` tinyint(1) NOT NULL DEFAULT '1',
  `req_acc_dt` datetime DEFAULT NULL,
  `req_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_shortlisted_profile`
--

CREATE TABLE `tt_shortlisted_profile` (
  `short_id` int(11) NOT NULL,
  `short_profile_id` int(11) DEFAULT NULL,
  `short_person_id` int(11) NOT NULL,
  `short_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_viewed_my_profile`
--

CREATE TABLE `tt_viewed_my_profile` (
  `vmp_id` int(11) NOT NULL,
  `vmp_profile_id` int(11) NOT NULL,
  `vmp_person_id` int(11) NOT NULL,
  `vmp_is_contacted` tinyint(1) NOT NULL DEFAULT '2',
  `vmp_viewed_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ct_pt_bas_reli_pref`
--
ALTER TABLE `ct_pt_bas_reli_pref`
  ADD PRIMARY KEY (`brp_id`);

--
-- Indexes for table `ct_pt_family_details`
--
ALTER TABLE `ct_pt_family_details`
  ADD PRIMARY KEY (`fd_id`);

--
-- Indexes for table `ct_pt_life_style`
--
ALTER TABLE `ct_pt_life_style`
  ADD PRIMARY KEY (`ls_id`);

--
-- Indexes for table `ct_pt_location`
--
ALTER TABLE `ct_pt_location`
  ADD PRIMARY KEY (`pl_id`);

--
-- Indexes for table `ct_pt_loc_pref`
--
ALTER TABLE `ct_pt_loc_pref`
  ADD PRIMARY KEY (`lp_id`);

--
-- Indexes for table `ct_pt_personal_info`
--
ALTER TABLE `ct_pt_personal_info`
  ADD PRIMARY KEY (`pi_id`);

--
-- Indexes for table `ct_pt_profile_photo`
--
ALTER TABLE `ct_pt_profile_photo`
  ADD PRIMARY KEY (`pp_id`);

--
-- Indexes for table `ct_pt_prof_pref`
--
ALTER TABLE `ct_pt_prof_pref`
  ADD PRIMARY KEY (`prof_pref_id`);

--
-- Indexes for table `ct_pt_reli_info`
--
ALTER TABLE `ct_pt_reli_info`
  ADD PRIMARY KEY (`ri_id`);

--
-- Indexes for table `mt_annual_income`
--
ALTER TABLE `mt_annual_income`
  ADD PRIMARY KEY (`ann_in_id`);

--
-- Indexes for table `mt_body_type`
--
ALTER TABLE `mt_body_type`
  ADD PRIMARY KEY (`bt_id`);

--
-- Indexes for table `mt_caste`
--
ALTER TABLE `mt_caste`
  ADD PRIMARY KEY (`caste_id`);

--
-- Indexes for table `mt_complexion`
--
ALTER TABLE `mt_complexion`
  ADD PRIMARY KEY (`cplx_id`);

--
-- Indexes for table `mt_currency`
--
ALTER TABLE `mt_currency`
  ADD PRIMARY KEY (`curr_id`);

--
-- Indexes for table `mt_dosam`
--
ALTER TABLE `mt_dosam`
  ADD PRIMARY KEY (`dosam_id`);

--
-- Indexes for table `mt_drinking_habit`
--
ALTER TABLE `mt_drinking_habit`
  ADD PRIMARY KEY (`dh_id`);

--
-- Indexes for table `mt_eating_habit`
--
ALTER TABLE `mt_eating_habit`
  ADD PRIMARY KEY (`eh_id`);

--
-- Indexes for table `mt_edu`
--
ALTER TABLE `mt_edu`
  ADD PRIMARY KEY (`edu_id`);

--
-- Indexes for table `mt_emp_type`
--
ALTER TABLE `mt_emp_type`
  ADD PRIMARY KEY (`emp_type_id`);

--
-- Indexes for table `mt_family_location`
--
ALTER TABLE `mt_family_location`
  ADD PRIMARY KEY (`f_loc_id`);

--
-- Indexes for table `mt_family_status`
--
ALTER TABLE `mt_family_status`
  ADD PRIMARY KEY (`fs_id`);

--
-- Indexes for table `mt_family_type`
--
ALTER TABLE `mt_family_type`
  ADD PRIMARY KEY (`ft_id`);

--
-- Indexes for table `mt_family_value`
--
ALTER TABLE `mt_family_value`
  ADD PRIMARY KEY (`fv_id`);

--
-- Indexes for table `mt_father_status`
--
ALTER TABLE `mt_father_status`
  ADD PRIMARY KEY (`fas_id`);

--
-- Indexes for table `mt_fav_music`
--
ALTER TABLE `mt_fav_music`
  ADD PRIMARY KEY (`fav_mu_id`);

--
-- Indexes for table `mt_hobbies_interest`
--
ALTER TABLE `mt_hobbies_interest`
  ADD PRIMARY KEY (`hobbies_id`);

--
-- Indexes for table `mt_language`
--
ALTER TABLE `mt_language`
  ADD PRIMARY KEY (`lan_id`);

--
-- Indexes for table `mt_marital_status`
--
ALTER TABLE `mt_marital_status`
  ADD PRIMARY KEY (`ms_id`);

--
-- Indexes for table `mt_mother_status`
--
ALTER TABLE `mt_mother_status`
  ADD PRIMARY KEY (`mos_id`);

--
-- Indexes for table `mt_occupation`
--
ALTER TABLE `mt_occupation`
  ADD PRIMARY KEY (`occ_id`);

--
-- Indexes for table `mt_physical_status`
--
ALTER TABLE `mt_physical_status`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `mt_premium_package`
--
ALTER TABLE `mt_premium_package`
  ADD PRIMARY KEY (`pp_id`,`pp_discount`);

--
-- Indexes for table `mt_profile`
--
ALTER TABLE `mt_profile`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `mt_profile_creator`
--
ALTER TABLE `mt_profile_creator`
  ADD PRIMARY KEY (`profile_creator_id`);

--
-- Indexes for table `mt_rasi_moon_sign`
--
ALTER TABLE `mt_rasi_moon_sign`
  ADD PRIMARY KEY (`rms_id`);

--
-- Indexes for table `mt_religion`
--
ALTER TABLE `mt_religion`
  ADD PRIMARY KEY (`religion_id`);

--
-- Indexes for table `mt_resident_status`
--
ALTER TABLE `mt_resident_status`
  ADD PRIMARY KEY (`rs_id`);

--
-- Indexes for table `mt_smoking_habit`
--
ALTER TABLE `mt_smoking_habit`
  ADD PRIMARY KEY (`sh_id`);

--
-- Indexes for table `mt_sports_fitness`
--
ALTER TABLE `mt_sports_fitness`
  ADD PRIMARY KEY (`sp_ft_id`);

--
-- Indexes for table `mt_star`
--
ALTER TABLE `mt_star`
  ADD PRIMARY KEY (`star_id`);

--
-- Indexes for table `mt_sub_caste`
--
ALTER TABLE `mt_sub_caste`
  ADD PRIMARY KEY (`sub_caste_id`);

--
-- Indexes for table `mt_zodiac_star_sign`
--
ALTER TABLE `mt_zodiac_star_sign`
  ADD PRIMARY KEY (`zodiac_id`);

--
-- Indexes for table `st_profile`
--
ALTER TABLE `st_profile`
  ADD PRIMARY KEY (`sett_id`);

--
-- Indexes for table `tt_mobile_no_viewed`
--
ALTER TABLE `tt_mobile_no_viewed`
  ADD PRIMARY KEY (`mv_id`);

--
-- Indexes for table `tt_premium_members`
--
ALTER TABLE `tt_premium_members`
  ADD PRIMARY KEY (`pm_id`);

--
-- Indexes for table `tt_pro_ig_by_me`
--
ALTER TABLE `tt_pro_ig_by_me`
  ADD PRIMARY KEY (`pig_id`);

--
-- Indexes for table `tt_request`
--
ALTER TABLE `tt_request`
  ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `tt_shortlisted_profile`
--
ALTER TABLE `tt_shortlisted_profile`
  ADD PRIMARY KEY (`short_id`);

--
-- Indexes for table `tt_viewed_my_profile`
--
ALTER TABLE `tt_viewed_my_profile`
  ADD PRIMARY KEY (`vmp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ct_pt_bas_reli_pref`
--
ALTER TABLE `ct_pt_bas_reli_pref`
  MODIFY `brp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_family_details`
--
ALTER TABLE `ct_pt_family_details`
  MODIFY `fd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_life_style`
--
ALTER TABLE `ct_pt_life_style`
  MODIFY `ls_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_location`
--
ALTER TABLE `ct_pt_location`
  MODIFY `pl_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_loc_pref`
--
ALTER TABLE `ct_pt_loc_pref`
  MODIFY `lp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_personal_info`
--
ALTER TABLE `ct_pt_personal_info`
  MODIFY `pi_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_profile_photo`
--
ALTER TABLE `ct_pt_profile_photo`
  MODIFY `pp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_prof_pref`
--
ALTER TABLE `ct_pt_prof_pref`
  MODIFY `prof_pref_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ct_pt_reli_info`
--
ALTER TABLE `ct_pt_reli_info`
  MODIFY `ri_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_annual_income`
--
ALTER TABLE `mt_annual_income`
  MODIFY `ann_in_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_body_type`
--
ALTER TABLE `mt_body_type`
  MODIFY `bt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mt_caste`
--
ALTER TABLE `mt_caste`
  MODIFY `caste_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=534;

--
-- AUTO_INCREMENT for table `mt_complexion`
--
ALTER TABLE `mt_complexion`
  MODIFY `cplx_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mt_currency`
--
ALTER TABLE `mt_currency`
  MODIFY `curr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_dosam`
--
ALTER TABLE `mt_dosam`
  MODIFY `dosam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_drinking_habit`
--
ALTER TABLE `mt_drinking_habit`
  MODIFY `dh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_eating_habit`
--
ALTER TABLE `mt_eating_habit`
  MODIFY `eh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_edu`
--
ALTER TABLE `mt_edu`
  MODIFY `edu_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_emp_type`
--
ALTER TABLE `mt_emp_type`
  MODIFY `emp_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mt_family_location`
--
ALTER TABLE `mt_family_location`
  MODIFY `f_loc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_family_status`
--
ALTER TABLE `mt_family_status`
  MODIFY `fs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mt_family_type`
--
ALTER TABLE `mt_family_type`
  MODIFY `ft_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_family_value`
--
ALTER TABLE `mt_family_value`
  MODIFY `fv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mt_father_status`
--
ALTER TABLE `mt_father_status`
  MODIFY `fas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mt_fav_music`
--
ALTER TABLE `mt_fav_music`
  MODIFY `fav_mu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_hobbies_interest`
--
ALTER TABLE `mt_hobbies_interest`
  MODIFY `hobbies_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `mt_language`
--
ALTER TABLE `mt_language`
  MODIFY `lan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `mt_marital_status`
--
ALTER TABLE `mt_marital_status`
  MODIFY `ms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mt_mother_status`
--
ALTER TABLE `mt_mother_status`
  MODIFY `mos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mt_occupation`
--
ALTER TABLE `mt_occupation`
  MODIFY `occ_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_physical_status`
--
ALTER TABLE `mt_physical_status`
  MODIFY `ps_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mt_premium_package`
--
ALTER TABLE `mt_premium_package`
  MODIFY `pp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_profile`
--
ALTER TABLE `mt_profile`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_profile_creator`
--
ALTER TABLE `mt_profile_creator`
  MODIFY `profile_creator_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mt_rasi_moon_sign`
--
ALTER TABLE `mt_rasi_moon_sign`
  MODIFY `rms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `mt_religion`
--
ALTER TABLE `mt_religion`
  MODIFY `religion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mt_resident_status`
--
ALTER TABLE `mt_resident_status`
  MODIFY `rs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mt_smoking_habit`
--
ALTER TABLE `mt_smoking_habit`
  MODIFY `sh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mt_sports_fitness`
--
ALTER TABLE `mt_sports_fitness`
  MODIFY `sp_ft_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mt_star`
--
ALTER TABLE `mt_star`
  MODIFY `star_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `mt_sub_caste`
--
ALTER TABLE `mt_sub_caste`
  MODIFY `sub_caste_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mt_zodiac_star_sign`
--
ALTER TABLE `mt_zodiac_star_sign`
  MODIFY `zodiac_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `st_profile`
--
ALTER TABLE `st_profile`
  MODIFY `sett_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_mobile_no_viewed`
--
ALTER TABLE `tt_mobile_no_viewed`
  MODIFY `mv_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_premium_members`
--
ALTER TABLE `tt_premium_members`
  MODIFY `pm_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_pro_ig_by_me`
--
ALTER TABLE `tt_pro_ig_by_me`
  MODIFY `pig_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_request`
--
ALTER TABLE `tt_request`
  MODIFY `req_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_viewed_my_profile`
--
ALTER TABLE `tt_viewed_my_profile`
  MODIFY `vmp_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
