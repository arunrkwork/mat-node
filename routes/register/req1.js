const express = require('express')
const router = express.Router() 
const fetch = require('node-fetch');

const bAuth = require('../../config/basicAuth')
const db = require('../../config/dbConnection')
const conPool = db.getPool()

const label  = ['profileCreator', 'maritalStatus', 'physicalStatus', 'languages'];
var queryString = [
    'select * from mt_profile_creator',
    'select * from mt_marital_status',
    'select * from mt_physical_status',
    'select * from mt_language',
] 
const BASE_URL = 'http://192.168.1.100:5001'

const LOGIN_URL = BASE_URL + '/users/login/'
const FORGOT_PASS_URL = BASE_URL + '/users/forgotPass/'
const CHANGE_PASS_URL = BASE_URL + '/users/changePass/'
const USER_AVAILABLE_URL = BASE_URL + '/users/available/'

const BODY_TYPE_URL = BASE_URL + '/bodyType'
const PROF_CRE_URL = BASE_URL + '/profileCreator'
const RELIGION_URL = BASE_URL + '/religion' 
const CASTE_URL = BASE_URL + '/caste'
const MARITAL_STATUS_URL = BASE_URL + '/maritalStatus'    
const PHY_STATUS_URL = BASE_URL + '/physicalStatus'
const FAMILY_STATUS_URL = BASE_URL + '/familyStatus'
const FAMILY_TYPE_URL = BASE_URL + '/familyType'
const FAMILY_VALUE_URL = BASE_URL + '/familyValue'
const EDUCATION_URL = BASE_URL + '/education'
const EMP_TYPE_URL = BASE_URL + '/empType'
const OCCUPATION_URL = BASE_URL + '/occupation'
const CURR_TYPE_URL = BASE_URL + '/currencyType'
const ANN_INCOME_URL = BASE_URL + '/annIncome' 
const LANGUAGES_URL = BASE_URL + '/languages' 

//router.use(bAuth.basicAuth)
    router.get('/data/', (req, res) => { 
      
        const url = [
      PROF_CRE_URL,
      MARITAL_STATUS_URL,
      PHY_STATUS_URL,
      LANGUAGES_URL,
      RELIGION_URL,
      FAMILY_VALUE_URL,
      FAMILY_TYPE_URL,
      FAMILY_STATUS_URL,
      EDUCATION_URL,
      EMP_TYPE_URL,
      OCCUPATION_URL,
      CURR_TYPE_URL,
      ANN_INCOME_URL
    ];
    
     
    Promise.all([
        fetch(url[0]).then(function(response){ return response.json() }),
       // fetch(url[1]).then(function(response){ return response.json() }),
       // fetch(url[2]).then(function(response){ return response.json() }),
       // fetch(url[3]).then(function(response){ return response.json() }),
       // fetch(url[4]).then(function(response){ return response.json() }),
       // fetch(url[5]).then(function(response){ return response.json() }),
        // fetch(url[6]).then(function(response){ return response.json() }),
        // fetch(url[7]).then(function(response){ return response.json() }),
        // fetch(url[8]).then(function(response){ return response.json() }),
        // fetch(url[9]).then(function(response){ return response.json() }),
        // fetch(url[10]).then(function(response){ return response.json() }),
        // fetch(url[11]).then(function(response){ return response.json() }),
        // fetch(url[12]).then(function(response){ return response.json() }),       
    ]).then(allResponses => {

    var data1 = allResponses[0];
    var data2 = allResponses[1]; 
    //console.log(allResponses)
    res.send(allResponses)
    }).catch(function(error) {
                /* on failure */
        res.send(error)
    })
    
    }) 

//export the router
module.exports=router;