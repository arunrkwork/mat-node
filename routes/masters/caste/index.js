const express = require('express')
const router = express.Router()  
 
const bAuth = require('../../../config/basicAuth')
const db = require('../../../config/dbConnection')
const conPool = db.getPool()
 
//router.use(bAuth.basicAuth)
router.get('/', (req, res) => {
     
    const selectQuery = `select * from mt_caste`;
    
    conPool.getConnection((err, connection) => {
        connection.query(selectQuery, (err, results) => {
            console.log(selectQuery);
            if(err) {
                console.log('err occoured');
            } else {
                res.send(results)
            }
        })
    })
})


router.get('/:id', (req, res) => {
    
    const id = req.params.id 
    const selectQuery = `select * from mt_caste where caste_reli_id = ?`;
    
    conPool.getConnection((err, connection) => {
        connection.query(selectQuery, [ id ], (err, results) => {
            console.log(selectQuery);
            if(err) {
                console.log('err occoured');
            } else {
                res.send(results)
            }
        })
    })
})

//export the router
module.exports=router;