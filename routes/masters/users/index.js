const express = require('express')
const router = express.Router() 
const jwt = require('jsonwebtoken')

const bAuth = require('../../../config/basicAuth')
const db = require('../../../config/dbConnection')
const conPool = db.getPool()

const mail = require('../../../shared/mail')
 
//router.use(bAuth.basicAuth)
router.post('/s1/', (req, res) => {

    const creatorId = req.body.creatorId
    const userName = req.body.userName
    const mobileNumber = req.body.mobileNumber

    var user = {
        "creatorId" : creatorId,
        "userName"  : userName,
        "mobileNumber" : mobileNumber
    }

   // localStorage.setItem("user", user)

    res.json(user)

})

router.post('/s2/', (req, res) => {
    
    const userName = req.body.userName; 
    const userPassword = req.body.userPassword; 
   
    const insertQuery = `insert into mt_user (user, pass) values (?, ?)`;
      
    conPool.getConnection((err, connection) => {
        connection.query(insertQuery, [
                userName,  
                userPassword], (error, results, fields) => {
            if(error) {
                console.log("failed to connect : " + error);
                res.send(JSON.stringify(error))
                return
            }         
            
            console.log(results.insertId);   
            //res.end(JSON.stringify(results))
            let payload = {subject: results.insertId}
            // let token = jwt.sign(payload, 'secretKey',{
            //     expiresIn: 15
            // })
            let token = jwt.sign(payload, 'secretKey')
            res.status(200).send({token})
        })
    })
})

router.post('/login/', (req, res) => {
    
    const userName = req.body.userName; 
    const userPassword = req.body.userPassword; 
   
    const selectQuery = `SET @userName = ?;
    SET @userPassword = ?; 
    SET @err = 0;
    SET @profileId = 0;
    CALL PC_CHECK_LOGIN(@userName, @userPassword, @err, @profileId);
    SELECT @err as err, @profileId as profileId;`;

      
    conPool.getConnection((err, connection) => {
        connection.query(selectQuery, [
                userName,  
                userPassword], (error, results, fields) => {
            
            let json = null

            if(error) {
                console.log("failed to connect : " + error);
                json = {
                    "error" : true,
                    "message" : "Sql Query Error",
                    "id" : null,
                    "sessionToken" : null
                } 
                res.send(json)
                return
            }         
          //  console.log(results);
            const err = results[5][0].err
            const profileId = results[5][0].profileId

            if(err == 1) {
                let payload = { subject: profileId } 
                let token = jwt.sign(payload, 'secretKey') 
                json = {
                    "error" : false,
                    "message" : "Login success",
                    "id" : profileId,
                    "sessionToken" : token
                } 
            } else if(err == 2) {
                json = {
                    "error" : true,
                    "message" : "Incorrect username and password",
                    "id" : null,
                    "sessionToken" : null
                } 
            } else {
                // none of the case
                json = {
                    "error" : true,
                    "message" : "Something went wrong", 
                    "id" : null,
                    "sessionToken" : null
                } 
            }
            
            res.status(200).send(json)
        })
    })
})

router.post('/reg/',  (req, res) => { 
    
    let proCreId = req.body.proCreId; 
    let pName = req.body.pName;  
    let pDob = req.body.pDob; 
    let pGender = req.body.pGender; 
    let pHeight = req.body.pHeight; 
    let pMobNo1 = req.body.pMobNo1; 
    let pEmail = req.body.pEmail; 
    let pPassword = req.body.pPassword; 
    let pMaritalStatusId = req.body.pMaritalStatusId; 
    let pPhysicalStatusId = req.body.pPhysicalStatusId; 
    let pMotherTongueId = req.body.pMotherTongueId;
    let riReligionId = req.body.riReligionId;
    let riCasteId = req.body.riCasteId;
    let riSubCaste = req.body.riSubCaste;
    if(!riSubCaste) riSubCaste = null;
    let fdFvId = req.body.fdFvId;
    if(!fdFvId) fdFvId = null;
    let fdFtId = req.body.fdFtId;
    if(!fdFtId) fdFtId = null;
    let fdFsId = req.body.fdFsId;
    if(!fdFsId) fdFsId = null;
    let piHighEduId = req.body.piHighEduId;
    if(!piHighEduId) piHighEduId = null;
    let piEmpTypeId = req.body.piEmpTypeId;
    if(!piEmpTypeId) piEmpTypeId = null;
    let piOccTypeId = req.body.piOccTypeId;
    if(!piOccTypeId) piOccTypeId = null;
    let piAnnIncomeCurrId = req.body.piAnnIncomeCurrId;
    if(!piAnnIncomeCurrId) piAnnIncomeCurrId = null;
    let piAnnIncomeId = req.body.piAnnIncomeId; 
    if(!piAnnIncomeId) piAnnIncomeId = null;
    console.log("name : " + req.body.key)
   
    const selectQuery = `SET @proCreId = ?;
    SET @pName = ?; 
    SET @pDob = ?; 
    SET @pGender = ?; 
    SET @pHeight = ?; 
    SET @pMobNo1 = ?; 
    SET @pEmail = ?; 
    SET @pPassword = ?; 
    SET @pMaritalStatusId = ?; 
    SET @pPhysicalStatusId = ?; 
    SET @pMotherTongueId =?;
    SET @riReligionId =?;
    SET @riCasteId =?;
    SET @riSubCaste =?;
    SET @fdFvId =?;
    SET @fdFtId =?;
    SET @fdFsId =?;
    SET @piHighEduId =?;
    SET @piEmpTypeId =?;
    SET @piOccTypeId =?;
    SET @piAnnIncomeCurrId =?;
    SET @piAnnIncomeId =?; 
    SET @err = 0;
    SET @profileId = 0;
    CALL PC_PROFILE_REG(@proCreId, 
        @pName, 
        @pDob, 
        @pGender, 
        @pHeight, 
        @pMobNo1,
        @pEmail, 
        @pPassword,
        @pMaritalStatusId, 
        @pPhysicalStatusId,
        @pMotherTongueId, 
        @riReligionId, 
        @riCasteId, 
        @riSubCaste, 
        @fdFvId, 
        @fdFtId, 
        @fdFsId, 
        @piHighEduId, 
        @piEmpTypeId, 
        @piOccTypeId, 
        @piAnnIncomeCurrId,
        @piAnnIncomeId,
        @err, 
        @profileId);
    SELECT @err as err, 
            @profileId as profileId;`;
      
    conPool.getConnection((err, connection) => {
        connection.query(selectQuery, [
                proCreId, 
                pName, 
                pDob,
                pGender,  
                pHeight,
                pMobNo1,  
                pEmail,
                pPassword,  
                pMaritalStatusId,
                pPhysicalStatusId,  
                pMotherTongueId,
                riReligionId,
                riCasteId,
                riSubCaste,
                fdFvId,
                fdFtId,
                fdFsId,
                piHighEduId,
                piEmpTypeId,
                piOccTypeId,
                piAnnIncomeCurrId,
                piAnnIncomeId], (error, results, fields) => {
            
            let json = null
            
            if(error) {
                console.log("failed to connect : " + error);
                json = {
                    "error" : true,
                    "message" : "Sql Query Error",
                    "id" : null,
                    "sessionToken" : null
                } 
                res.send(json)
                return
            }          

            const err = results[25][0].err
            const profileId = results[25][0].profileId
                       
            if(err == 1) {

                let payload = {subject: profileId}
            
                let token = jwt.sign(payload, 'secretKey')
                json = {
                    "error" : false,
                    "message" : "Registration Success",
                    "id" : profileId,
                    "sessionToken" : token
                } 
            } else  if(err == 2) {
                json = {
                    "error" : true,
                    "message" : "Mobile number exists",
                    "id" : null,
                    "sessionToken" : null
                }
            } else  if(err == 3) {                 
                json = {
                    "error" : true,
                    "message" : "Email exists",
                    "id" : null,
                    "sessionToken" : null
                }
            } else {
                // none of the case
                json = {
                    "error" : true,
                    "message" : "Something went wrong", 
                    "id" : null,
                    "sessionToken" : null
                } 
            }

            res.status(200).send(json)
            
        })
    })
})

router.post('/changePass/', (req, res) => {

    const profileId = req.body.profileId
    const userPassword = req.body.newPassword
 
    const selectQuery = `SET @profileId = ?;
    SET @userPassword = ?;
    SET @err = 0;
    SET @resultCode = 0;
    CALL PC_UPDATE_PASSWORD(@profileId, @userPassword, @err, @resultCode);
    SELECT @err as err, @resultCode as resultCode;`;

    conPool.getConnection((err, connection) => {
        connection.query(selectQuery, [
            profileId,
            userPassword  
        ], (error, results, fields) => {

            let json = null

            if(error) {
                console.log("failed to connect : " + error);
                json = {
                    "error" : true,
                    "message" : "Sql Query Error", 
                    "resultCode" : null
                } 
                res.send(json)
                return
            }

            const err = results[5][0].err
            const resultCode = results[5][0].resultCode
 
            if(err == 1) {
                // update success
                json = {
                    "error" : false,
                    "message" : "Update Success", 
                    "resultCode" : resultCode
                } 
            } else if(err == 2) {
                // update failed
                json = {
                    "error" : true,
                    "message" : "Update Failed", 
                    "resultCode" : resultCode
                } 

            } else if(err == 3) {
                // profile id not exists
                json = {
                    "error" : true,
                    "message" : "Profile Doesn't Exists", 
                    "resultCode" : resultCode
                } 
            } else {
                // none of the case
                json = {
                    "error" : true,
                    "message" : "Something went wrong", 
                    "resultCode" : resultCode
                } 
            }
            
            res.status(200).send(json)

        })
    })

})

router.post('/forgotPass/', (req, res) => {

    const userName = req.body.userName 
 
    const selectQuery = `SET @userName = ?; 
    SET @err = 0;
    SET @profileId = 0;
    SET @email = '0';
    CALL PC_GET_PROFILE_ID_EMAIL(@userName, @err, @profileId, @email);
    SELECT @err as err, @profileId as profileId, @email as email;`;

    conPool.getConnection((err, connection) => {
        connection.query(selectQuery, [
            userName 
        ], (error, results, fields) => {

            let json = null

            if(error) {
                console.log("failed to connect : " + error);
                json = {
                    "error" : true,
                    "message" : "Sql Query Error", 
                    "resultCode" : null
                } 
                res.send(json)
                return
            } else {
                const err = results[5][0].err
                const profileId = results[5][0].profileId
                const email = results[5][0].email

                if(err == 1) {
                    // success
                    json = {
                        "error" : false,
                        "message" : "Success", 
                        "resultCode" : profileId,
                        "email" : email
                    } 
                    const to = email
                    const subject = 'KV Matrimony Forgot Password'
                    const message = 'http://localhost:5002/change-password/' + profileId
                    mail.sendMail(to, subject, message) 

                } else if(err == 2) {
                    // failed
                    json = {
                        "error" : true,
                        "message" : "Failed", 
                        "resultCode" : null,
                        "email" : null
                    } 

                } else {
                    // none of the case
                    json = {
                        "error" : true,
                        "message" : "Something went wrong", 
                        "resultCode" : null,
                        "email" : null
                    } 
                }
            }
 
            res.status(200).send(json)

        })
    })

})

router.post('/available/', (req, res) => {

    const userName = req.body.userName 
     
    const selectQuery = `SET @userName = ?; 
        SET @err = 0;
        SET @isAvailable = 0;
        CALL PC_CHECK_USER_AVAILABLE(@userName, @err, @isAvailable);
        SELECT @err as err, @isAvailable as isAvailable;`;
    //console.log(req.body);
    conPool.getConnection((err, connection) => {
        connection.query(selectQuery, [
                userName 
            ], (error, results, fields) => {
    
                let json = null 
    

                if(error) {
                    console.log("failed to connect : " + error);
                    json = {
                        "error" : true,
                        "message" : "Sql Query Error", 
                        "isAvailable" : null
                    } 
                    res.send(json)
                    return
                }

                const err = results[4][0].err
                const isAvailable = results[4][0].isAvailable
    
                if(err == 1) {
                    // success
                    json = {
                        "error" : false,
                        "message" : "Success", 
                        "isAvailable" : isAvailable
                    } 
                } else if(err == 2) {
                    // failed
                    json = {
                        "error" : true,
                        "message" : "Failed", 
                        "isAvailable" : isAvailable
                    } 
    
                } else {
                    // none of the case
                    json = {
                        "error" : true,
                        "message" : "Something went wrong", 
                        "resultCode" : profileId
                    } 
                } 

                res.status(200).send(json)
    
        })
    })

})

//export the router
module.exports = router;