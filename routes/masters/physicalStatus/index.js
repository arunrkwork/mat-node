const express = require('express')
const router = express.Router()  
 
const bAuth = require('../../../config/basicAuth')
const db = require('../../../config/dbConnection')
const conPool = db.getPool()
 
//router.use(bAuth.basicAuth)
router.get('/', (req, res) => { 
   
    const selectQuery = `select * from mt_physical_status`;
      
    conPool.getConnection((err, connection) => {
        connection.query(selectQuery, (err, results) => {
            if(err) {
                console.log('err occoured');
            } else {
                res.send(results)
            }
        })
    })
})

//export the router
module.exports=router;