const express = require('express')
const router = express.Router() 
 
const bAuth = require('../../../config/basicAuth')
const db = require('../../../config/dbConnection')
const conPool = db.getPool()
 
//router.use(bAuth.basicAuth)
router.get('/', (req, res) => { 
   
    const selectQuery = `select * from mt_profile_creator`;
      
    conPool.getConnection((err, connection) => {
        connection.query(selectQuery, (err, results) => {
            if(err) {
                console.log('err occoured');
            } else {
                if(res.statusCode == 200)
                    res.send(results)
                    else console.log('err status code : ' + res.statusCode)
            }
        })
    })
})

//export the router
module.exports=router;