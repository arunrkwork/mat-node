const mysql = require('mysql');
const config = require('./config')

var pool = null;
module.exports = {
    getPool: function() {
        if(pool) return pool;
        pool = mysql.createPool(config)
        return pool
    }
}