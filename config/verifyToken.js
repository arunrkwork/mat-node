const jwt = require('jsonwebtoken')

var token = function verifyToken(req, res, next) {
    if(!req.headers.authorization) {  
      return res.status(401).send('Unauthorized request')
    }
    let token = req.headers.authorization.split(' ')[1]
    if(token === 'null') {
      return res.status(401).send('Unauthorized request')    
    }
    // let payload = jwt.verify(token, 'secretKey', )
    // if(!payload) {
    //   return res.status(401).send('Unauthorized request')    
    // }
    // req.userId = payload.subject
    // next()
    // verify secret and checks exp
		jwt.verify(token, 'secretKey', function (err, currUser) {
			if (err) {
				res.send(err);
			} else {
				// decoded object
				req.userId = currUser;
				next();
			}
		});
  }

  exports.verifyToken = token