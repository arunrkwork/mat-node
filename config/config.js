let config = {
    connectionLimit : 1000,
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'matrimony',
    multipleStatements : true, 
    server : {
        address : "192.168.1.100",
        port : "5001"
    }
};

module.exports = config;