const basicAuth = require('express-basic-auth') 

var auth = basicAuth({
    users: { 'admin': 'supersecret' },
    unauthorizedResponse: getUnauthorizedResponse
})
  
var getUnauthorizedResponse = function getUnauthorizedResponse(req) {
    return req.auth
        ? ('Credentials ' + req.auth.user + ':' + req.auth.password + ' rejected')
        : 'No credentials provided'
}
exports.basicAuth = auth
