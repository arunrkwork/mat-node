const serverConfig = require('../server/config/config')
const express= require("express")
const bodyparser= require("body-parser")
const morgan = require('morgan')
const cors = require('cors')
const app = express() 

const token = require('./config/verifyToken')

const users = require('./routes/masters/users')
const bodyType = require('./routes/masters/bodyType')
const profileCreator = require('./routes/masters/profileCreator')
const religion = require('./routes/masters/religion')
const caste = require('./routes/masters/caste')
const maritalStatus = require('./routes/masters/maritalStatus')
const physicalStatus = require('./routes/masters/physicalStatus')
const familyStatus = require('./routes/masters/familyStatus')
const familyType = require('./routes/masters/familyType')
const familyValue = require('./routes/masters/familyValue')
const education = require('./routes/masters/education')
const empType = require('./routes/masters/empType')
const occupation = require('./routes/masters/occupation')
const currencyType = require('./routes/masters/currencyType')
const annIncome = require('./routes/masters/annIncome')
const languages = require('./routes/masters/languages')

const req1 = require('./routes/register/req1')
//set JSON as MIME type
app.use(bodyparser.json());

//front-end not sending any form data
app.use(bodyparser.urlencoded({extended:false}));

app.use(morgan('short'))
app.use(express.static('./public'))

app.use(function (req, res, next) { 
  
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
  
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
  
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');
    res.setHeader('Content-Type', 'application/json');
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  
    // Pass to next layer of middleware
    next();
  });
  app.disable('etag');
app.use(cors())
app.use('/users', users) 
app.use('/bodyType', bodyType)
app.use('/profileCreator', profileCreator)
app.use('/religion', religion)
app.use('/caste', caste)
app.use('/maritalStatus', maritalStatus)
app.use('/physicalStatus', physicalStatus)
app.use('/familyStatus', familyStatus)
app.use('/familyType', familyType)
app.use('/familyValue', familyValue)
app.use('/education', education)
app.use('/empType', empType)
app.use('/occupation', occupation)
app.use('/currencyType', currencyType)
app.use('/annIncome', annIncome)
app.use('/languages', languages) 

app.use('/req1', req1) 


//app.use(token.verifyToken)
  
//Assign port number
var server = app.listen(serverConfig.server.port, 
  serverConfig.server.address, 
  () => {

  var host = server.address().address
  var port = server.address().port
  console.log("app listening at http://%s:%s", host, port)

});